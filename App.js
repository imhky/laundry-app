
import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import TabNav from './Components/NavTab';
import Services from './Components/Services';
import Login from "./Components/Login";
import ServiceDetails from './Components/ServiceDetails';
import Notifications from './Components/Notifications';
import NotificationsDhobi from './Components/NotificationsDhobi';
import UserProfile from './Components/UserProfile';
import OrderList from './Components/OrderList'
import Register from './Components/Register';
import RegisterCustomer from './Components/RegisterCustomer';
import FirstPage from './Components/firstPage'
import TopTab from './Components/TopTab.js'
import OrderSucc from './Components/OrderSucc'
import LoginDhobi from './Components/LoginDhobi'
import NavTabCustomer from './Components/NavTabCustomer'
import createService from './Components/createService'
import updateService from './Components/updateService'
import DobiProfile from './Components/DobiProfile';
import Dashboard from './Components/Dashboard';
import { Provider } from 'react-redux';
import {store, persistor} from './Redux/Store';
import { persistStore } from "redux-persist"
import { PersistGate } from "redux-persist/integration/react"
// import TopTab from './Components/TopTab.js'
import Map from './Components/Map'
import ConfirmCodeUser from './Components/ConfirmCodeUser.js'
import ConfirmCodeDhobi from './Components/ConfirmCodeDhobi.js'
import forgotPasswordEmailForUser from './Components/forgotPasswordEmailForUser.js'
import ConfirmCodeForForgotPasswordUser from './Components/ConfirmCodeForForgotPasswordUser'
import newPasswordForUserField from './Components/newPasswordForUserField.js'
import PushNotificationTest from './Components/pushNotificationTest'
import MapCL from './Components/MapCL';
import DhobiSelection2 from './Components/DhobiSelection2';
const Stack = createNativeStackNavigator();
function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
    <NavigationContainer>
      <Stack.Navigator  screenOptions={{header: () => null}}>

      {/* <Stack.Screen name="PushNotificationTest" component={PushNotificationTest} /> */}
      <Stack.Screen name="FirstPage" component={FirstPage} />
      <Stack.Screen name="MapCL" component={MapCL} />
      <Stack.Screen name="forgotPasswordEmailForUser" component={forgotPasswordEmailForUser} />
      <Stack.Screen name="newPasswordForUserField" component={newPasswordForUserField} />
      <Stack.Screen name="ConfirmCodeForForgotPasswordUser" component={ConfirmCodeForForgotPasswordUser} />
      <Stack.Screen name="ConfirmCodeUser" component={ConfirmCodeUser} />
      <Stack.Screen name="ConfirmCodeDhobi" component={ConfirmCodeDhobi} />
      <Stack.Screen name="LoginDhobi" component={LoginDhobi} />
      <Stack.Screen name="OrderSucc" component={OrderSucc} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="RegisterCustomer" component={RegisterCustomer} />
      <Stack.Screen name="NavTabCustomer" component={NavTabCustomer} />
      <Stack.Screen name="Login" component={Login} />
      {/* <Stack.Screen name="TopTab" component={TopTab} /> */}
      <Stack.Screen name="DhobiSelection2" component={DhobiSelection2} />
      <Stack.Screen name="TabNav" component={TabNav} />
      <Stack.Screen name="createService" component={createService} />
      <Stack.Screen name="updateService" component={updateService} />
    
      <Stack.Screen name="Services" component={Services} />
      <Stack.Screen name="Notifications" component={Notifications} />
      <Stack.Screen name="Notification" component={NotificationsDhobi} />
      <Stack.Screen name="UserProfile" component={UserProfile} />
      <Stack.Screen name="OrderList" component={OrderList} />
      <Stack.Screen name="DobiProfile" component={DobiProfile} />
      <Stack.Screen name="Dashboard" component={Dashboard} />
        <Stack.Screen name="ServiceDetails" component={ServiceDetails} />
        <Stack.Screen name="TopTab" component={TopTab} />
      
      </Stack.Navigator>
    </NavigationContainer>
    </PersistGate>
    </Provider>
  );
}

export default App;