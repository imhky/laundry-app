import * as React from 'react';
import {SafeAreaView, View, StyleSheet, Image, TextInput , Pressable, Text  } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialIcons';

const OrderSucc = ({navigation}) => {
    return(
        <SafeAreaView style={{
            backgroundColor: 'white',
            marginTop:'7%',
            height: '100%'
        }}>
        
             <View style={{
                 width: '100%',
                 backgroundColor: '#0987E3',
                 height: 50,
                
                //  borderBottomStartRadius: 100,
                 marginBottom:'3%'
             }}>
                 <Icon style={{  
                 justifyContent: 'center',
                 alignItems: 'center',
                 color: 'white',
                 margin:10
               }}
                 name="arrow-back"
                 size={30}
                 onPress={()=>navigation.navigate('TabNav')}
                //  onPress={()=>navigation.goBack()}
               />

             </View>

             <Image source={require("../Components/images/orderSucc.png")}
                style={style.image} />
            
          
             <Text style={{
                     color: 'black',
                     fontFamily:'sans-serif',
                     fontSize: 36,
                    fontWeight:'bold',
                     marginTop:'5%',
                     textAlign: 'center',
                     marginBottom:'3%'
                   
                 }}>Order Succssfull</Text>
   <Text style={{
                     color: 'black',
                     fontFamily:'sans-serif',
                     fontSize: 28,
                     marginTop:'3%',
                     textAlign: 'center',
                     marginBottom:'3%'
                   
                 }}>Thank you!</Text>

<Text style={{
                     color: '#816464',
                     fontFamily:'sans-serif',
                     fontSize: 15,
                     marginLeft:'13%',
                     marginTop:'3%',
                     marginRight: '13%'
                 }}>         You’ll get a confirmation email soon once it get reach to Admin, so meets at your doostep!.</Text>
          

        </SafeAreaView>
       
    )
}

const style = StyleSheet.create({
    image:{
     // width: 136,
     width:191,
  //height: 138,
  height:162,
  left: "26%",
  top: "7%",
  marginBottom: '16%'
  
    }
});

export default OrderSucc;