import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  View,
  StatusBar,
  Text,
  TextInput,
  FlatList,
  Dimensions,
  StyleSheet,
  Image,
  Pressable,
  ScrollView,
  LogBox,
  ToastAndroid
} from 'react-native';
import { ActivityIndicator, Button } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import { setDhobiLoginDetails } from '../Redux/Slice';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Axios from 'axios';
import baseURL from './BaseURL/baseURL';
import * as ImagePicker from 'expo-image-picker';

import Ionicons from '@expo/vector-icons/Ionicons';

import { getApps, initializeApp } from "firebase/app";
import { getStorage, ref, uploadBytes, getDownloadURL } from "firebase/storage";
import uuid from "uuid";

const firebaseConfig = {
    apiKey: "AIzaSyC5QHD_X7RO3RzOZzK_PsYYmVBu6QAF9nA",
    authDomain: "laundryapp-15598.firebaseapp.com",
    projectId: "laundryapp-15598",
    storageBucket: "laundryapp-15598.appspot.com",
    messagingSenderId: "359858181338",
    appId: "1:359858181338:web:37bac919dcf0f65c7fc1e3",
    measurementId: "G-2LY7XDDQ7V"
  };
  
  // Editing this file with fast refresh will reinitialize the app on every refresh, let's not do that
  if (!getApps().length) {
    initializeApp(firebaseConfig);
  }
  
  // Firebase sets some timeers for a long period, which will trigger some warnings. Let's turn that off for this example
  LogBox.ignoreLogs([`Setting a timer for a long period`]);


const DobiProfile=({navigation})=>{
  const dispatch = useDispatch();
  const { loginDhobi } = useSelector((state) => state.loginDhobi);
  const dhobiID = loginDhobi._id
 
  const [loading, setLoading] = React.useState(true)
  const [loading2, setLoading2] = React.useState(true)
  const [Inc, setIncr] = React.useState(0)
  const [image, setImage] = React.useState(loginDhobi.profilePic);
  const [uploading, setUplaoding] = useState(false)

  console.log('P Pic:::', loginDhobi.profilePic)
  
  React.useEffect(() => {
    Axios.get(`${baseURL}/imageUpload/admin/photo/${loginDhobi._id}`).then(res => {
        console.log('get Profile::::::', res)
        setImage(res.data.profilePic)
      }).catch(err => console.log(err))

    // Axios.get(`${baseURL}/orderManipulate/totalOrderesList/${loginDhobi._id}`).then(res => {
    //   // const resp = JSON.stringify(res.data)
    //   console.log('ALL Orders of Dhobi::::::', res.data.orderRelate)
    //   if (res.data.length === 0 || res.data === undefined || res.data === []) {
    //     console.log('No Order')
    //     ToastAndroid.showWithGravity(
    //       "You don't have any Completed Order!",
    //       ToastAndroid.SHORT,
    //       ToastAndroid.CENTER
    //     );
    //     // setData([])
    //     setLoading(false)
    //     setLoading2(false)
    //   } else {
    //     console.log('Orders ::D:::', res.data)
    //     // setData(res.data)
    //     setLoading(false)
    //     // setLoading2(false)
    //   }
 
    // })
    // .catch(err => console.log(err))
 
   },[])

   const tConv24 = (time24) => {
    var ts = time24;
    var H = +ts.substr(0, 2);
    var h = (H % 12) || 12;
    h = (h < 10)?("0"+h):h;  // leading 0 at the left for 1 digit hours
    var ampm = H < 12 ? " AM" : " PM";
    ts = h + ts.substr(2, 3) + ampm;
    return ts;
  }
  const uploadImageToNodeJS = (url) => {
    Axios.put(`${baseURL}/imageUpload/admin/uploadPhoto/${loginDhobi._id}`, {
      profileImage: url
    })
   .then(res => {
   console.log('Image uploaded :::::::', res.data)

             if (res.data.result === "Profile Uploaded successfully!") {

               ToastAndroid.showWithGravity(
                 `Profile Uploaded successfully!`,
                 ToastAndroid.SHORT,
                 ToastAndroid.CENTER 
               );
               dispatch(setDhobiLoginDetails({
                Result: loginDhobi.Result,
                __v: loginDhobi.__v,
                _id: loginDhobi._id,
                accessToken: loginDhobi.accessToken,
                address: loginDhobi.address,
                createdAt: loginDhobi.createdAt,
                email:loginDhobi.email,
                isService: loginDhobi.isService,
                mobile_no: loginDhobi.mobile_no,
                updatedAt: loginDhobi.updatedAt,
                username: loginDhobi.username,
                profilePic: url,
                frequency_order: loginDhobi.frequency_order
              })); 
               setImage(url)
             } else {
                ToastAndroid.showWithGravity(
                    `Profile did not Upload, Please try again`,
                    ToastAndroid.SHORT,
                    ToastAndroid.CENTER
                  );
             }
     
 }).catch(err => {
     console.log('ERROR :::: ', err)
 })
   

}
   
   
  const handleImagePicked = async (pickerResult) => {
    try {
      setUplaoding(true)

      if (!pickerResult.cancelled) {
        ToastAndroid.showWithGravity(
          `Profile is being uploading!`,
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
        const uploadUrl = await uploadImageAsync(pickerResult.uri);
       console.log('Imaged Upload, url is:', uploadUrl)
       setImage(uploadUrl)
       uploadImageToNodeJS(uploadUrl)
      }
    } catch (e) {
      console.log(e);
      alert("Upload failed, Please try again!");
    } finally {
        setUplaoding(false)
    }
  };
  
  const pickImage = async () => {
    // No permissions request is necessary for launching the image library
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 4],
      quality: 1,
    });

    console.log('Complete Result:::',result);

    console.log(result);
    handleImagePicked(result);
    // if (!result.cancelled) {
    //   setImage(result.uri);
    // }
  };


  
    return(
        <SafeAreaView style={{
            flex:1,
            backgroundColor:"white"
        }}>
            {//Parent Portion Starts
            }
        <View style={{
            width:'100%',
           // height: 170,
           height:120,
            backgroundColor: "rgba(9, 135, 227, 0.74)"
        }}>
            {// Inner portion one Starts
            }
           <View style={{
               flexDirection:"column",
               justifyContent:"space-between",
               alignItems:"center"
           }}>
         <Button mode="outlined"  labelStyle={{ color: "white", fontSize: 12 }} color="rgba(9, 135, 227, 0.74)" 
                
                style={{
                  width: 98,
                  height: 28,
                  left:'33%',
                   top: 8,
                   backgroundColor:"rgba(1, 122, 209, 0.75)",
                   marginHorizontal: 20,
                   borderWidth: 0.5,
                   borderColor:"rgba(1, 122, 209, 0.75)",
               borderRadius:50,
                   justifyContent: 'center',
                   alignItems: 'center',
                }} >
                 Support
                </Button>
      
        <Button mode="outlined"  labelStyle={{ color: "white", fontSize: 12 }} color="rgba(9, 135, 227, 0.74)"  onPress={() => {
            dispatch(setDhobiLoginDetails({}));
            navigation.navigate('FirstPage')
          }}
                
                style={{
                 
                  width: 98,
                  height: 28,
                left:'33%',
                 top: 15,
                 shadowColor:'red',
                 backgroundColor:"rgba(254, 8, 8, 0.75)",
                 marginHorizontal: 20,
                 borderWidth: 0.5,
             borderRadius:50,
             borderColor:"#E31A1A",
                 justifyContent: 'center',
                 alignItems: 'center',
                 color:'white'
                }} >
                 Logout
                </Button>
    
           </View>
           {
               //Inner portion one ends
           }
           {
               //Inner portion 2nd starts 
           }
           <View style={{
               flexDirection:"row",
               justifyContent:"space-between",
               alignItems:"center"
           }}>
                 {(image === null || image === '' || image === undefined) ? <Image source={require("../Components/images/pic6.png")}
               style={{
                width: 100,
                height: 100,
                left: 23,
                top: 10,
               }} /> : <Image source={{ uri: image }}
              
               style={{
                width: 100,
                height: 100,
                left: 23,
                top: 10,
                borderRadius:50
               }} /> }
              <TouchableOpacity style={{
                  marginTop:30,
                  marginLeft:-10
              }} onPress={pickImage}>
               <View style={{
                  marginLeft:0
              }}>
                   
                 <Ionicons style={{
               borderWidth:0.2,
               borderRadius:20,
               backgroundColor: 'white',
               borderColor:'rgba(9, 135, 227, 0.74)',
               padding:3
           }} name={'camera'} size={20} color='rgba(9, 135, 227, 0.74)' />
               </View></TouchableOpacity> 
               <Text style={{
                   width: 218,
                   height: 29,
                   top:0,
                   left:'15%',
                   fontStyle: "normal",
                   fontWeight: "normal",
                   fontSize: 16,
                   lineHeight: 19,
                   color: "#FFFFFF"
                   
               }}>{loginDhobi.username}</Text>
           </View>

           {/* <View  style={{
               flexDirection:"row",
             
               alignItems:"center"
           }}>
               
           {(image === null || image === '' || image === undefined) ? <Image source={require("../Components/images/pic6.png")}
               style={{
                width: 100,
                height: 100,
                left: 23,
                top: -25,
               }} /> : <Image source={{ uri: image }}
              
               style={{
                width: 100,
                height: 100,
                left: 23,
                top: -25,
                borderRadius:50
               }} /> }
           <TouchableOpacity onPress={pickImage}><Ionicons style={{
               borderWidth:0.2,
               borderRadius:20,
               backgroundColor: 'white',
               borderColor:'rgba(9, 135, 227, 0.74)',
               padding:3
           }} name={'camera'} size={20} color='rgba(9, 135, 227, 0.74)' /></TouchableOpacity> 
         
           </View> */}
           {
               //Inner portion 2nd Ends
           }
                {
            //3rd parent portion starts
        }
        <View style={{
            flexDirection:"row",
        }}>
             <Image source={require("../Components/images/pic8.png")}
               style={{
                width: 30,
                height: 20,
                left: 23,
                top: 30,
               }} />
               <Text style={{
                   width: 218,
                   height: 29,
                   left: 33,
                   top: 32,
                   fontStyle: "normal",
                   fontWeight: "normal",
                   fontSize: 15,
                   lineHeight: 19,                  
                   color: "#846262"
               }}>{loginDhobi.email}</Text>
        </View>
        {
            //3rd Parent portion ends
        }
        {
            //4rth Parent portion starts
        }
        <View style={{
            flexDirection:"row",
        }}>
             <Image source={require("../Components/images/pic9.png")}
               style={{
                width: 30,
                height: 30,
                left: 20,
                top: 38,
               }} />
               <Text style={{
                   width: 218,
                   height: 29,
                   left: 28,
                   top: 43,
                   fontStyle: "normal",
                   fontWeight: "normal",
                   fontSize: 15,
                   lineHeight: 19,                  
                   color: "#846262"
               }}>{loginDhobi.mobile_no}</Text>
        </View>
        {
            //4rth Portion ends
        }
        {
            //5th portion starts
        }
        <View style={{
            flexDirection:"row",
        }}>
             <Image source={require("../Components/images/pic10.png")}
               style={{
                width: 35,
                height: 48,
                left: 20,
                top: 50,
               }} />
               <Text style={{
                   width: 218,
                   height: 40,
                   left: 28,
                   top:55,
                   fontStyle: "normal",
                   fontWeight: "normal",
                   fontSize: 13,
                   lineHeight: 19,                  
                   color: "#846262"
               }}>{loginDhobi.address}.</Text>
        </View>
           {
               //Inner portion 3rd Starts 
           }
           
           {
               //Inner portion 3rd Ends 
           }
        </View>
        {
            //1st Parent Portion ends Here
        }
        {
            //2nd Parent Portion Starts
        }
        <View>
            {
                //Inner 1st Portion Starts
            }
            <View style={{
                flexDirection:"row",
                justifyContent:"space-between",
                alignItems:"center"
            }}>
              {/* <Text style={{
                  width: 108,
                  height: 45,
                  left: '95%',
                  top:80,
                  fontStyle: "normal",
                  fontWeight: "bold",
                  fontSize: 20,
                  lineHeight: 19,
                  color: "rgba(1, 122, 209, 0.75)"
              }}>09</Text>
             <Text style={{
                  width: 108,
                  height: 45,
                  left: '95%',
                  top:80,
                  fontStyle: "normal",
                  fontWeight: "bold",
                  fontSize: 20,
                  lineHeight: 19,
                  color: "rgba(1, 122, 209, 0.75)"
              }}>03</Text>
              <Text style={{
                  width: 108,
                  height: 45,
                  left: '90%',
                  top: 80,
                  fontStyle: "normal",
                  fontWeight: "bold",
                  fontSize: 20,
                  lineHeight: 19,
                  color: "rgba(1, 122, 209, 0.75)"
              }}>07</Text> */}
            </View>
            {
                //1st Inner portion done
            }
            {
                //2nd Inner Portion Starts
            }
            <View style={{
                width:340,
                height:90,
                left: '12%',
                flexDirection:"row",
                justifyContent:"space-between",
                alignItems:"center",
                // borderBottomWidth: 1,
                borderBottomColor: 'rgba(1, 122, 209, 0.75)',
            }}>
                {/* <Text style={{
                      width: 118,
                      height: 45,
                      left: 0,
                      top: 35,
                      fontStyle: "normal",
                      fontWeight: "normal",
                      fontSize: 14,
                      lineHeight: 19,
                      color: "#000000"
                }}>Total Orders
                </Text>
                <Text style={{
                  width: 118,
                  height: 45,
                  left: 25,
                  top:35,
                  fontStyle: "normal",
                  fontWeight: "normal",
                  fontSize: 14,
                  lineHeight: 19,
                  color: "#000000"
              }}>Inprogress</Text>
              <Text style={{
                  width: 118,
                  height: 45,
                  left: 40,
                  top:35,
                  fontStyle: "normal",
                  fontWeight: "normal",
                  fontSize: 14,
                  lineHeight: 19,
                  color:"#000000"
              }}>Completed</Text> */}
            </View>
            {
                //2nd Inner Portion Ends
            }
        </View>
        {
            //2nd Parent Portion Ends
        }
        {
            //3rd parent Portion starts
        }
        {/* <View style={{
            width: '94%',
            height: 36,
            left: '3%',
            right: '3%',
            top: 5,
            backgroundColor: "#4AFF5C",
            justifyContent:"center",
            alignItems:"center"
        }}>
          <Text style={{
            textAlign:"center",
            width: 173,
            height: 36,
            marginTop:10,
            fontStyle: "normal",
            fontWeight: "bold",
            fontSize: 17,
            lineHeight: 23,
            color: "#FFFFFF"
          }}>Pending Orders</Text>
          </View> 
          {
            //3rd parent Portion ends
        }
          {
              //4rth Parent portion starts
          }
          
          <View style={{
                   borderWidth:0.5,
                   borderColor:"#584A4A",
                   marginTop:10,
                   width: '94%',
                  height: 130,
                  borderRadius: 10,  
                    left:'3%',
                    right: '3%'
               }}>
                   {
              // 1st Inner view starts
          }
           <View style={{
               width: '90%',
               height: 140,
           flexDirection:"row",
           justifyContent:"space-between",
         }}>
             <Image source={require("../Components/images/pic5.png")}
             style={{
                width: 32,
                height: 32,
                left: 10,
                top: 15,
             }} />
           <Text style={{
             position:"absolute",
             width: 245,
             height: 36,
             left: 50,
             top: 10,
             fontStyle: "normal",
             fontWeight: "bold",
             fontSize: 12,
             lineHeight: 20,
             color: "#584A4A"
           }}>Order No. 00876
           </Text>
           <Text style={{
             position:"absolute",
             width: 245,
             height: 36,
             left:50,
             top: 25,
             fontStyle: "normal",
             fontWeight: "bold",
             fontSize: 12,
             lineHeight: 20,
             color: "#584A4A"
           }}>Haris Khan Yousafzai
           </Text>
           <Text style={{
             position:"absolute",
             width: 245,
             height: 36,
             left:50,
             top: 39,
             fontStyle: "normal",
             fontWeight: "bold",
             fontSize: 12,
             lineHeight: 20,
             color: "#584A4A"
           }}>hky@gmail.com
           </Text>
           <Pressable>
          <View style={{
           width: 98,
           height: 28,
           left:178,
            top: 20,
            backgroundColor:"rgba(1, 122, 209, 0.75)",
            marginHorizontal: 20,
            borderWidth: 0.5,
            borderColor:"rgba(1, 122, 209, 0.75)",
        borderRadius:50,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            <Text style={{color: 'white'}}>Accepted</Text>
          </View>
        </Pressable>
        <Pressable>
          <View style={{
           width: 76,
           height: 28,
           left:40,
            top: 55,
            backgroundColor:"rgba(254, 8, 8, 0.75)",
            marginHorizontal: 20,
            borderWidth: 0.5,
        borderRadius:50,
        borderColor:"#E31A1A",
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            <Text style={{color: 'white'}}>Cancel</Text>
          </View>
        </Pressable>
        <Text style={{
             position:"absolute",
             width: 245,
             height: 36,
             left:15,
             top: 60,
             fontStyle: "normal",
             fontWeight: "bold",
             fontSize: 14,
             lineHeight: 20,
             color: "rgba(1, 122, 209, 0.75)"
           }}>Total : Rs. 500   
           </Text>
           <Text style={{
             position:"absolute",
             width: 245,
             height: 36,
             left:15,
             top: 75,
             fontStyle: "normal",
             fontWeight: "bold",
             fontSize: 14,
             lineHeight: 20,
             color: "black"
           }}>Date: 10/10/2021  
           </Text>
           </View>
           {
              // 1st Inner view ends
          }
           {
              // 2nd Inner view starts
          }
           <View style={{
               flexDirection:"row",
               justifyContent:"space-between",
               alignItems:"center"
           }}>
               <Image source={require("../Components/images/pic7.png")}
             style={{
                width: 20,
                height: 25,
                left: 10,
                top:-50,
             }} />
             <Text style={{
                 width: 245,
                 height: 43, 
                 top:-40,
                 right:70,                
                 fontStyle: "normal",
                 fontWeight: "normal",
                 fontSize: 11,
                 lineHeight: 13,
                 color: "#846262"
             }}>Mohallah Burh hujjra, Cheena Rustam, Mardan, KPK.</Text>
           </View>
           {
              // 2nd Inner view ends
          }
           </View> */}
           {
               //4rth parent portion Ends
           }
{
    //5th parent portion starts
}
<Button mode="outlined" color="rgba(9, 135, 227, 0.74)" onPress={()=>navigation.navigate('Dashboard')}
                
                style={{
                    borderColor: 'rgba(9, 135, 227, 0.74)',
                    margin:10,
                    marginTop:'30%'
                }} >
                 View Dashboard
                </Button>
{
    //6th parent portion starts
}
    
           {
    //6th parent portion ends
}
        </SafeAreaView>
    )
}
export default DobiProfile;


async function uploadImageAsync(uri) {
    // Why are we using XMLHttpRequest? See:
    // https://github.com/expo/expo/issues/2402#issuecomment-443726662
    const blob = await new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function () {
        resolve(xhr.response);
      };
      xhr.onerror = function (e) {
        console.log(e);
        reject(new TypeError("Network request failed"));
      };
      xhr.responseType = "blob";
      xhr.open("GET", uri, true);
      xhr.send(null);
    });
  
    const fileRef = ref(getStorage(), uuid.v4());
    const result = await uploadBytes(fileRef, blob);
  
    console.log('RESULT::::::::::', result)
    // We're done with the blob, close and release it
    blob.close();
  
    return await getDownloadURL(fileRef);
  }
  