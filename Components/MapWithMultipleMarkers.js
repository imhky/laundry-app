import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import MapView, { Marker } from 'react-native-maps'
import * as React from 'react';
export default function Map() {

  const [region, setRegion] = React.useState({
    latitude: 34.1986,   
      longitude: 72.0404,  
      latitudeDelta: 0.0922,  
      longitudeDelta: 0.0421,  
    })

    // const [markers, setMarkers] = React.useState({
    //   latitude: 34.1986,   
    //     longitude: 72.0404,  
    //     latitudeDelta: 0.0922,  
    //     longitudeDelta: 0.0421,  
    //   })

      const markers = [{
        title: 'Ghaffar Kaka',
        coordinates: {
          latitude: 34.214487392988026,
          longitude: 72.04260092228651
        },
      },
      {
        title: 'Dhobi Laundry Clg Chowk',
        coordinates: {
          latitude: 34.18965409126425,
          longitude: 72.0222607254982
        },  
      }]
    const [MarkersCordinate, setMarkersCordinate] = React.useState([])

      const onMapPress = (e) => {
        console.log("coordinates:", JSON.stringify(e.nativeEvent.coordinate));
        alert("coordinates:" + JSON.stringify(e.nativeEvent.coordinate));
        console.log('COORDINATE:::::', e.nativeEvent.coordinate)
        setMarkers(e.nativeEvent.coordinate)
        setMarkersCordinate(e.nativeEvent.coordinate)
      }
  return (
    <View style={styles.container}>
      <Text>Open up App.js to start working on your app!</Text>
      <MapView provider={MapView.PROVIDER_GOOGLE}
      style={StyleSheet.absoluteFillObject}
      // style={styles.mapStyle}  
      showsUserLocation={false}  
      zoomEnabled={true}  
      zoomControlEnabled={true}  
      initialRegion={region}
      onPress={onMapPress}
      >
        {markers.map(marker => (
          <Marker 
            coordinate={marker.coordinates}
            title={marker.title}
          />
        ))}
           {/* <Marker  
            coordinate={markers}  
            title={"JavaTpoint"}  
            description={"Java Training Institute"}  
          />  */}
        </MapView>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});
