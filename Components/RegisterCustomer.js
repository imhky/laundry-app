import * as React from 'react';
import {  View, StyleSheet, TextInput , Pressable, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, ToastAndroid, Platform} from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { Text } from 'react-native-elements';
import { CheckBox } from 'react-native-elements/dist/checkbox/CheckBox';
import CheckBoxIcon from 'react-native-elements/dist/checkbox/CheckBoxIcon';
import { Input } from 'react-native-elements/dist/input/Input';
import { Button } from 'react-native-elements';
import { SafeAreaView } from 'react-native-web';
import { ActivityIndicator, Caption, Checkbox } from 'react-native-paper';
import Services from './Services';
import { Formik } from 'formik'
import * as yup from 'yup'
import baseURL from './BaseURL/baseURL';
import Icon from 'react-native-vector-icons/Ionicons';


export default function Register({navigation}) {
  const [checked, setChecked] = React.useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = React.useState(false)
  const [loading, setLoading] = React.useState(false)

  const [iconNpass, setIconNpass] = React.useState({
    icon: "eye-off",
    passwordSecure: true
  })

  const changeIcon = () => {
    const IC = iconNpass.icon === 'eye' ? 'eye-off' : 'eye'
    const P = !iconNpass.passwordSecure
    setIconNpass({
      icon:IC,
      passwordSecure: P
    })
  }

  const signUpValidationSchema = yup.object().shape({
    fullName: yup
      .string()
      .matches(/(\w.+\s).+/, 'Enter at least 2 names')
      .required('Full name is required'),
    phoneNumber: yup
      .string()
      .matches(/(03)(\d){9}\b/, 'Enter a valid phone number')
      .required('Phone number is required'),
    email: yup
      .string()
      .email("Please enter valid email")
      .required('Email is required'),
    password: yup
      .string()
      .matches(/\w*[a-z]\w*/,  "Password must have a small letter")
      // .matches(/\w*[A-Z]\w*/,  "Password must have a capital letter")
      .matches(/\d/, "Password must have a number")
      // .matches(/[!@#$%^&*()\-_"=+{}; :,<.>]/, "Password must have a special character")
      .min(8, ({ min }) => `Password must be at least ${min} characters`)
      .required('Password is required'),
    confirmPassword: yup
      .string()
      .oneOf([yup.ref('password')], 'Passwords do not match')
      .required('Confirm password is required'),
      address: yup
      .string()
      .matches(/(\w.+\s).+/, 'Enter at least 2 names')
      .required('Exact Address is required')
  })
  return (
      <ScrollView>
         {loading ? (<ActivityIndicator size="large" color="#0f9df7" style={{ height:800}} />) : ( 
   <View style={styles.container}>
    
      <View style={{
        flexDirection: 'row'
       
      }}>
      <View>
      <View style={{
                 width: 200,
                 backgroundColor: '#0987E3',
                 height: 200,
                 opacity:0.8,
                borderRadius:100,
                marginLeft: -60,
                marginTop: -85
               
             }}>
            </View>
            <View style={{
                marginTop: -160,
                 width: 200,
                 backgroundColor: '#0987E3',
                 height: 200,
              opacity:0.8,
                borderRadius:100,
                marginLeft: -90
               
             }}>
            </View>
      </View>
      <Pressable>
      <View>
      <Text style={styles.text1}>Sign Up</Text>
      </View>
      </Pressable>
          
       </View>
       <Formik
        validationSchema={signUpValidationSchema}
            initialValues={{
              fullName: '',
              email: '',
              phoneNumber: '',
              password: '',
              confirmPassword: '',
              address: ''
            }}
            onSubmit={values => {
              setLoading(true)
              setIsButtonDisabled(true)
              console.log('Values::::::', values)
         
              setLoading(true)
              fetch(`${baseURL}/userAuth/sendEmail`, {
                method:"POST",
                headers:{
                    "Content-Type":"application/json"
                },
                body:JSON.stringify({
                        email:values.email,
                        username:values.fullName
                })
            }).then(res => res.json())
            .then(res => {
              setIsButtonDisabled(true)
              console.log('REGISTER RES Email:::::::', res)
                if (res.Result === "User already exist in this email") {
                    console.log(res)
                         ToastAndroid.showWithGravity(
                          "User already exist with this email!",
                          ToastAndroid.SHORT,
                          ToastAndroid.CENTER
                        );
                        setLoading(false)
                } else  if (res.Result === "User already exist in this username") {
                  console.log(res)
                       ToastAndroid.showWithGravity(
                        "User already exist with this username!",
                        ToastAndroid.SHORT,
                        ToastAndroid.CENTER
                      );
                     
                      setLoading(false)
              } else if (res.message === "Enter the verification code that send to your email") {
                  console.log(res)
                       ToastAndroid.showWithGravity(
                       "Please Add Your Location!",
                        ToastAndroid.SHORT,
                        ToastAndroid.CENTER
                      );
                      navigation.navigate('MapCL', {
                        email:values.email,
                        password:values.password,
                        username:values.fullName,
                        mobile_no:values.phoneNumber,
                        address: values.address
                       })
                      setLoading(false)
              }else  {
                  ToastAndroid.showWithGravity(
                    "Somthing Went Wrong!",
                    ToastAndroid.SHORT,
                    ToastAndroid.CENTER
                  );
        
                  setLoading(false)
                }
             
            }).catch(err => {
                console.log('ERROR :::: ', err)
            })
            setTimeout(() => {
              setIsButtonDisabled(false)
            }, 3000)
            }}
          >
            {({ 
               handleChange,
               handleBlur,
               handleSubmit,
               values,
               errors,
               isValid,
             }) => (
              <>
       <View >
      <TextInput
        style={styles.field1}
        onChangeText={handleChange('email')}
        onBlur={handleBlur('email')}
        placeholder='example@gmail.com'
        underlineColorAndroid ='transparent'
        value={values.email}
        keyboardType="email-address"
        />
      {errors.email &&
         <Text style={{ fontSize: 10, color: 'red', marginLeft:'10%' }}>{errors.email}</Text>
       }
      </View>
      <View >
      <TextInput
        style={styles.field1}
        onChangeText={handleChange('fullName')}
        onBlur={handleBlur('fullName')}
        value={values.fullName}
       
        placeholder='Full Name'
        underlineColorAndroid ='transparent'
        />
         {errors.fullName &&
         <Text style={{ fontSize: 10, color: 'red', marginLeft:'10%' }}>{errors.fullName}</Text>
       }
      
      </View>
      
      <View >
      <TextInput
        style={styles.field1}
        onChangeText={handleChange('phoneNumber')}
        onBlur={handleBlur('phoneNumber')}
        value={values.phoneNumber}
        placeholder='0344-0110318'
        underlineColorAndroid ='transparent'
        />
           {errors.phoneNumber &&
         <Text style={{ fontSize: 10, color: 'red', marginLeft:'10%' }}>{errors.phoneNumber}</Text>
       }
      
      </View>
      <View >
      <TextInput
        style={styles.field1}
        onChangeText={handleChange('address')}
        onBlur={handleBlur('address')}
        value={values.address}
       
        placeholder='Exact Location'
        underlineColorAndroid ='transparent'
        />
         {errors.address &&
         <Text style={{ fontSize: 10, color: 'red', marginLeft:'10%' }}>{errors.address}</Text>
       }
      
      </View>
      <View style={{
       flexDirection:'row'
     }}>
      <TextInput
        style={styles.field2}
        onChangeText={handleChange('password')}
        onBlur={handleBlur('password')}
        value={values.password}
        secureTextEntry={iconNpass.passwordSecure}
        placeholder='Password'
        width='90%'
        underlineColorAndroid ='transparent'
        />
         <Icon name={iconNpass.icon} 
          onPress={() => changeIcon()}
           size={25} style={{
             marginLeft: '-20%',
             top:13,
          }} color="#0987E3" />
      
      </View>
      {errors.password &&
         <Text style={{ fontSize: 10, color: 'red', marginLeft:'10%' }}>{errors.password}</Text>
       }
      {/* <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}> */}
         <View style={{
       flexDirection:'row'
     }}>
      <TextInput
        style={styles.field2}
        onChangeText={handleChange('confirmPassword')}
        onBlur={handleBlur('confirmPassword')}
        value={values.confirmPassword}
        secureTextEntry={iconNpass.passwordSecure}
        width='90%'
        placeholder='Confirm Password'
        underlineColorAndroid ='transparent'
        />
         <Icon name={iconNpass.icon} 
          onPress={() => changeIcon()}
           size={25} style={{
             marginLeft: '-20%',
             top:13,
          }} color="#0987E3" />
      
      </View>
      {errors.confirmPassword &&
         <Text style={{ fontSize: 10, color: 'red', marginLeft:'10%' }}>{errors.confirmPassword}</Text>
       }
      {/* </TouchableWithoutFeedback>
    </KeyboardAvoidingView> */}
    
   <View style={{flexDirection: 'row'}}>
   <View style={{flexDirection: 'row', marginTop: 5, marginLeft: 30}}>
   <Checkbox
      status={checked ? 'checked' : 'unchecked'}
      onPress={() => {
        setChecked(!checked);
      }}
      // onValueChange={handleChange('checkBox')}
      color="#0f9df7"
    />
     
      </View>
     <View>
     <Text style={styles.termAndCon}> By signing up you accept <Text style={{color: 'skyblue'}}>Term of </Text></Text>
     <Text style={{color: 'skyblue', marginLeft:5}}> Service and Privacy Policy</Text>
     </View>
   </View>
   
 
     <View style={{marginTop: 40}}>
 
        <TouchableOpacity onPress={() => {
          if (checked === true) {
            handleSubmit()
          } else {
            ToastAndroid.showWithGravity(
              "Accept our Terms of Service & Privacy Policy!",
              ToastAndroid.SHORT,
              ToastAndroid.CENTER
            );
       
          }
        
        }}  disabled={!isValid || isButtonDisabled} >
          {/* onPress={()=>navigation.navigate('LoginDhobi')} */}
        <View style={{
            height: 38,
            marginHorizontal: 20,
            borderWidth: 1,
            borderColor: (!isValid || isButtonDisabled === true ? '#c4c4c4' : '#0f9df7'),
            borderRadius:50,
           
            backgroundColor: (!isValid || isButtonDisabled === true ? '#c4c4c4' : '#0f9df7'),
            width:'89%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            <Text style={{fontSize: 18, color: 'white'}}>Register</Text>
          </View>
        
        </TouchableOpacity>
     </View>
     </>
            )}
          </Formik>
   
      {/* onPress={()=>navigation.navigate('LoginDhobi')} */}
     <View style={{flexDirection: 'row', justifyContent: 'center', marginVertical:'3%'}}>
         <Text style={styles.foot}>Already have an account? </Text>
        <Pressable onPress={()=>navigation.navigate('LoginDhobi')}><Text style={styles.foot2}>Login</Text></Pressable> 
     </View>

     
   </View>)}
   </ScrollView>
   
  );
}

const styles = StyleSheet.create({
    text1: {
        padding: 5,
        fontSize: 36,
        marginLeft: '30%',
        marginTop: 80,
        marginBottom: 70,
        fontWeight: 'bold'
        

    },
    field1 : {
        borderWidth:  1,
        borderColor:  'skyblue',
       borderRadius: 50,
       paddingLeft:30,
       paddingBottom:4,
       marginTop:5,
       marginLeft:20,
       marginRight:20,
       marginBottom:10,
       height:45,
       fontSize: 19

       
    },
    field2 : {
        borderWidth:  1,
        borderColor:  'skyblue',
        borderRadius: 50,
        paddingLeft:30,
        paddingBottom:4,
        marginTop:5,
        marginLeft:20,
        marginRight:20,
        marginBottom:10,
        height:45,
        fontSize: 19
        
     },
     termAndCon: {
      fontSize:15,
      fontWeight: 'bold',
      paddingLeft:5,
      marginTop: 10,
       marginLeft: 0
     },
     forget: {
        fontSize:13,
        color: 'grey'
     },
    
     foot: {
        padding: 5,
        fontSize: 15,
        marginTop: 10,
       textAlign: 'center',
       color: 'grey'
     },
     foot2: {
      padding: 5,
      fontSize: 15,
      marginTop: 10,
     textAlign: 'center',
     color: 'blue'
   },
  container: {
    flex: 1,
    backgroundColor: 'white',
    height: 950,
  
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  container: {
    flex: 1
  },
  textInput: {
    height: 10,
    borderColor: "#000000",
    borderBottomWidth: 1,
    marginBottom: 36
  },
})