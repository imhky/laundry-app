import { useDispatch, useSelector } from 'react-redux';


const Message = () => {
  const dispatch = useDispatch();
  const { message } = useSelector((state) => state.message);

  const handlePress = () => {
    dispatch(setMessage('Message from Component'));
  };

  return (
    <View style={styles.container}>
      <Text style={styles.text}>{message}</Text>
      <Button title={'Set Message'} onPress={handlePress} />
    </View>
  );
};

export default Message;