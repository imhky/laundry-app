import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import MapView, { Marker } from 'react-native-maps'
import * as React from 'react';
import * as Location from "expo-location";

export default function Map2() {

  const [region, setRegion] = React.useState({
    latitude: 34.1986,   
      longitude: 72.0404,  
      latitudeDelta: 0.0922,  
      longitudeDelta: 0.0421,  
    })

    const [markers, setMarkers] = React.useState({
      latitude: 34.1986,   
        longitude: 72.0404,  
        latitudeDelta: 0.0922,  
        longitudeDelta: 0.0421,  
      })
      const [location, setLocation] = React.useState(null);
    const [MarkersCordinate, setMarkersCordinate] = React.useState([])

      const onMapPress = (e) => {
        console.log("coordinates:", JSON.stringify(e.nativeEvent.coordinate));
        alert("coordinates:" + JSON.stringify(e.nativeEvent.coordinate));
        console.log('COORDINATE:::::', e.nativeEvent.coordinate)
        setMarkers(e.nativeEvent.coordinate)
        setMarkersCordinate(e.nativeEvent.coordinate)
      }

      const getLoc =() =>{
        //console.log("press",location.coords)
        let latitude= location.coords.latitude
        let longitude=location.coords.longitude
        setRegion({
                  latitude: latitude,
                  longitude: longitude,
                  latitudeDelta: 0.015,
                  longitudeDelta: 0.0121,
                })
      }

      React.useEffect(() => {
        (async () => {
          let { status } = await Location.requestForegroundPermissionsAsync();
          if (status !== 'granted') {
            setErrorMsg('Permission to access location was denied');
          }
    
          let location = await Location.getCurrentPositionAsync({});
          setLocation(location);
        })();
        // getLoc()
      }, []);



  return (
    <View style={styles.container}>
      <Text>Open up App.js to start working on your app!</Text>
      <MapView provider={MapView.PROVIDER_GOOGLE}
      style={StyleSheet.absoluteFillObject}
      // style={styles.mapStyle}  
      showsUserLocation={false}  
      zoomEnabled={true}  
      zoomControlEnabled={true}  
      initialRegion={region}
      onPress={onMapPress}
      >
           <Marker  
            coordinate={markers}  
            title={"Mardan"}  
            description={"Ghaffar Kaka Laundry Services"}  
          /> 
        </MapView>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});
