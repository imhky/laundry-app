import React from 'react';
import {
  SafeAreaView,
  View,
  StatusBar,
  Text,
  FlatList,
  Dimensions,
  StyleSheet,
  Image,
  Pressable,
  ScrollView,
  Platform,
  KeyboardAvoidingView,
  KeyboardAvoidingViewBase,
  TouchableWithoutFeedback,
  Keyboard,
  ToastAndroid
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import DatePicker from 'react-native-datepicker'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { ActivityIndicator, Button, RadioButton, TextInput } from 'react-native-paper';
import Ionicons from '@expo/vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { DT } from './datePicker';
import baseURL from './BaseURL/baseURL';
import { useDispatch, useSelector } from 'react-redux';

const ServiceDetails = ({route, navigation}) => {
  const [checked, setChecked] = React.useState('first');
  // const [date, setDate] = React.useState('2016-05-15')
  const [date, setDate] = React.useState(new Date(1598051730000));
  const [time, setTime] = React.useState(4);
  const [mode, setMode] = React.useState('date');
  const [loading, setLoading] = React.useState(false)
  const { login } = useSelector((state) => state.login);

   const userID = login._id
  const [show, setShow] = React.useState(false);
  const { 
    // userID,
    dhobiID,
    NormalPrice,
    UrgentPrice,
    img,
    title,
    description
   } = route.params;
   console.log('Customer id:', userID)
   
  const [pickUpLoc, setPickUpLoc] = React.useState('')
  // const onChange = (event, selectedDate) => {
  //   const currentDate = selectedDate || date;
  //   setShow(Platform.OS === 'ios');
  //   if (mode === 'date') {
  //     setDate(currentDate);
  //   } else if (mode === 'time') {
  //     setTime(currentDate)
  //   }
    
  // };
  const onChange = (event, selectedValue) => {
    setShow(Platform.OS === 'ios');
    if (mode == 'date') {
      const currentDate = selectedValue || new Date();
      setDate(currentDate);
 
    } else if (mode == 'time') {
      const selectedTime = selectedValue || new Date();
      setTime(selectedTime);

    }
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
    setMode(currentMode)
  };
  console.log('DAte:::', date)
  console.log('Time:::', time)
  const [isButtonDisabled, setIsButtonDisabled] = React.useState(false)

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };

  const placeOrder = () => {
    setIsButtonDisabled(true)
    setLoading(true)
   console.log('Loc::::::', pickUpLoc)
    fetch(`${baseURL}/orderManipulate/newOrder`, {
      method:"POST",
      headers:{
          "Content-Type":"application/json"
      },
      body:JSON.stringify({
        customer_id:userID,
        admin_id:dhobiID,
        order_type: checked === "first" ? 'NORMAL' : 'URGENT',
        order_status: 'PENDING',
        order_address: pickUpLoc,
        order_pickDate:date,
        order_pickTime: time,
        order_price: checked === "first" ? NormalPrice : UrgentPrice
        // NormalPrice,
        // UrgentPrice,
        // img,
        // title,
        // description
      })
  }).then(res => res.json())
  .then(res => {
    console.log('Place Order:::::::', res)
      if (res.Result === "order placed successfully") {
          console.log(res)
               ToastAndroid.showWithGravity(
                "Your Order Placed successfully!",
                ToastAndroid.SHORT,
                ToastAndroid.CENTER
              );
              navigation.navigate('OrderSucc')
              setLoading(false)
      }  else  {
        ToastAndroid.showWithGravity(
          "Somthing Went Wrong!",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
        // navigation.navigate('Dashboard')
        setLoading(false)
      }
   
  }).catch(err => {
      console.log('ERROR :::: ', err)
  })
  setTimeout(() => {
    setIsButtonDisabled(false)
  }, 3000)
  }
  

  
  
  return (
    <ScrollView>
      <SafeAreaView style=
      {{flex:1 , 
      backgroundColor:"white",
      marginTop:Platform.OS==="android"?25:0}}>
      
       
        <Icon style={{  
                 justifyContent: 'center',
                 alignItems: 'center',
                 marginTop:13,
                 marginLeft:15
               }}
                 name="arrow-back"
                 size={30}
                 onPress={()=>navigation.goBack()}
               />
        
      <View style={{
       
        marginTop:"3%",
       width:"97%",
       marginLeft:"1.6%",
       // borderWidth: 1,
       // borderColor: "#CCC8C8",
        borderRadius:10,
        boxSizing: 'border-box',
        borderWidth:1,
        borderColor: 'grey',
        height: 650
        
      }}>
        
           
        <Image source={{uri:`${img}`}}
                style={style.image} />
         
        
        <Text style={{
          position:"absolute",
          width: "auto",
          height: "auto",
          left: "23%",
         // top: 165,
         top:"28.5%",
          fontStyle: "normal",
          fontWeight: "bold",
          fontSize: 17,
          lineHeight: 20,
          color: "#584A4A",
          
        }}>{title}</Text>    
        <Text style={{
          position:"absolute",
          width: "80%",
          height: "auto",
          left: "8%",
          top: "33.5%",
          fontStyle: "normal",
          fontWeight: "normal",
          fontSize: 13,
          lineHeight: 16,
          color: "#777171", 
        }}>{description}</Text>  
        
        <View style={style.Rectangle}>
          <Text style={{
            textAlign:"center",
            width: "auto",
            height: "auto",
            marginTop:"-1%",
            fontStyle: "normal",
            fontWeight: "bold",
            fontSize: 17,
            lineHeight: 23,
            color: "#FFFFFF",
            
            
          }}>Normal Delviery</Text>
          
          </View> 
      
          <Text style={{
              width: "70%",
              height: "auto",
              left:"25%",
              top: "17%",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: 16,
              lineHeight: 27,
              color: "rgba(9, 135, 227, 0.74)",
              
          }}>
            Rs. {NormalPrice} / Item (48 hours)
          </Text>
         
          <View style={style.Rectangle1}>
          <Text style={{
            textAlign:"center",
            width: 173,
            height: 36,
            marginTop:10,
            fontStyle: "normal",
            fontWeight: "bold",
            fontSize: 17,
            lineHeight: 23,
            color: "#FFFFFF"
          }}>Urgent Delviery</Text>
          </View>  
          <Text style={{
              width: "70%",
              height: "auto",
              left: "25%",
              top: "18.5%",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: 16,
              lineHeight: 27,
              color: "rgba(9, 135, 227, 0.74)",
              
          }}>
            Rs. {UrgentPrice} / Item (8 hours)
          </Text>
          <View style={{
            marginTop:"27%",
            height:"auto",
                           
            
            }}>
            <View style={{
               flexDirection:"row",
               justifyContent:"space-between",
               marginTop:"6%",
               alignItems:"center",
               marginLeft:"6%",
               marginRight:"5%",
               
              
               

            }}>
      <RadioButton style={{
        width: 18,
        height: 18,
        left: 39,
        top: 469,
      }}
        value="first"
        status={ checked === 'first' ? 'checked' : 'unchecked' }
        onPress={() => setChecked('first')}
      />
     <Text style={{
       left:"-35%",
       color: "#604F4F",
       fontWeight:"bold",
       fontStyle:"normal"
       }}>
         Normal Delivery
         </Text>
      <RadioButton style={{
        width: 19,
        height: 19,
        left: 130,
        top: 808,
       
      }}
        value="second"
        status={ checked === 'second' ? 'checked' : 'unchecked' }
        onPress={() => setChecked('second')}
      />
     <Text style={{
       left:"-35%",
       color: "#604F4F",
       fontWeight:"bold",
       fontStyle:"normal",
       
     }}>
       Urgent Delivery
       </Text>
    </View>
    </View>
    
    {/* <View style={{
      top:"3%",
      
     
    }}>
      <View style={style.DeliveryInformation}>
        <Text style={{
                 color: "#604F4F",
                 fontWeight:"bold",
                 fontStyle:"normal"
        }}>
          Pick up Date
          </Text>
        <Text 
            style={{
              color: "#604F4F",
              fontWeight:"bold",
              fontStyle:"normal"
     }} >
          Pick up Time
          </Text>
        <Text
          style={{
            color: "#604F4F",
            fontWeight:"bold",
            fontStyle:"normal"
   }}>
          Pick up Location
          </Text>
      </View> 
    </View> */}
     
     {/* <KeyboardAvoidingView  behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={style.container}
      >
         <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
         </TouchableWithoutFeedback>
        </KeyboardAvoidingView> */}

      <View style={{
        flexDirection:"row",
        justifyContent:"space-between",
        alignItems:"center",
        marginLeft:"3%",
        marginRight:"5%",
        marginTop:"5%"
      }}>
          <View style={{flexDirection:"column", justifyContent: 'space-around'}}>
        <Button  mode="outlined" color="#0987E3"  onPress={showDatepicker} 
         style={{
                    borderColor: '#0987E3',
                    margin:5
                    
                }}
                >Pick up Date</Button>
             
        
      {show && (
        <DateTimePicker 
          testID="dateTimePicker"
          value={date}
          mode={mode}
          is24Hour={true}
          display="default"
          onChange={onChange}
        />
      )}
        <Button  mode="outlined" color="#0987E3"   onPress={showTimepicker}
         style={{
                    borderColor: '#0987E3',
                    margin:5
                }}
                >Pick up Time</Button>
               
      </View>
   
        <View style={{
          width:'65%'
        }}>
        <TextInput 
                     label="Pick Up Location"
                     mode="outlined"
                     underlineColor='#0987E3'
                     activeOutlineColor='#0987E3'
                     activeUnderlineColor='#0987E3'
                     selectionColor='#0987E3'
                     outlineColor='#0987E3'
                     style={{marginLeft:'3%', width:'85%'}}
                     value={pickUpLoc}
                     onChangeText={pickUpLoc =>  setPickUpLoc(pickUpLoc)}
                     placeholder='Enter Your Exact Location'
               />
                  
        </View>
   
      </View>
    
      <Text style={{
        width: "89%",
        height: "7%",
        left: "7.5%",
        top:45,
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: 14,
        lineHeight: 16,
        color: "#201C1C",
       
      }}>Includes Pickup and Delivery (No Extra Charges)</Text>
     </View>
   
     <View style={{
       flexDirection:"row",
       justifyContent:"space-between",
       alignItems:"center",
       height:60
       }}>
        <TouchableOpacity disabled={isButtonDisabled} onPress={()=> {
        
            placeOrder()
        }}> 
          <View style={{
            height: 45,
            backgroundColor: isButtonDisabled === true ? 'grey' : "rgba(9, 135, 227, 0.74)",
            marginHorizontal: 55,
            borderWidth: 1,
            borderColor: isButtonDisabled === true ? 'grey' : "rgba(9, 135, 227, 0.74)",
            borderRadius:5,
            width:260,
            justifyContent: 'center',
            alignItems: 'center',
            top:0
          }}>
            <Text style={{
            color: 'white',
            fontStyle:"normal",
            fontWeight:"bold",
            fontSize: 19,
            lineHeight: 26
            // {loading ? (<ActivityIndicator size="medium" color="white" style={{paddingTop:20}}/>) : 'Order Now'}
                }} > Order Now  </Text>
          </View>
        </TouchableOpacity>
        
     </View>
 
      </SafeAreaView> 
      </ScrollView>
  
    );
  };
  
  const style = StyleSheet.create({
    container: {
      flex: 1
    },
    image:{
     // width: 136,
     width:135,
  //height: 138,
  height:150,
  left: "33%",
  top: "3%",
  
    },
  Rectangle:{
  width: "89%",
  height: "8%",
  left: "6%",
  top: "16.5%",
  backgroundColor: "rgba(9, 135, 227, 0.74)",
  justifyContent:"center",
  alignItems:"center"
    },

    Rectangle1:{
        width: "89%",
        height: "8%",
        left: "6%",
        top: "17.9%",
        backgroundColor: "rgba(9, 135, 227, 0.74)",
        justifyContent:"center",
        alignItems:"center"
          },
         /* RadioButton:{
            flexDirection:"row",
            justifyContent:"space-between",
            marginTop:155,
            alignItems:"center"
            
            
          }
          */
         DeliveryInformation:{
          flexDirection:"row",
          justifyContent:"space-between",
          alignItems:"center",
          marginLeft:"3%",
          marginRight:"4%"
          
         }
    
  });
export default ServiceDetails;