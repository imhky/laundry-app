import React, { useState } from 'react';
import {
  SafeAreaView,
  View,
  StatusBar,
  Text,
  FlatList,
  Dimensions,
  StyleSheet,
  Image,
  Pressable,
  ScrollView,
  Platform,
  ToastAndroid,
  RefreshControl
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import  Axios  from 'axios';
import baseURL from './BaseURL/baseURL';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ServiceDetails from './ServiceDetails';
import {TextInput, Checkbox, Avatar, Button} from 'react-native-paper';
const NotificationsDhobi = ({navigation}) => {
  const [loading, setLoading] = React.useState(true)
  const [data, setData] = React.useState([])
  const [Inc, setIncr] = React.useState(0)
  const { loginDhobi } = useSelector((state) => state.loginDhobi);
  const dhobiID = loginDhobi._id

  const loadUserData = () => {
    setLoading(true)
    setIncr(Inc + 1)
  }
  
  React.useEffect(() => {
    //6182c88cfbc7fa6a18da5f0d
    Axios.get(`${baseURL}/notifications/allNotifications/${dhobiID}`).then(res => {

      console.log('ALL Notification Dhobi::::::', res.data)
      if (res.data.length === 0 || res.data === undefined || res.data === []) {
        console.log('No Notification')
        ToastAndroid.showWithGravity(
          "You don't have any Notification yet!",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
        // navigation.navigate('TabNav')
        setLoading(false)
        setData([])
       
      } else {
        console.log('Notification :::::', res.data)
        setData(res.data)
        setLoading(false)
      }

    })
    .catch(err => console.log(err))
   },[Inc])

  return(
    
       <SafeAreaView style=
       {{
         flex:1 , 
         backgroundColor:"white"
         
         }}>
           
           <View style={{
              top:0,
                width:'100%',
                backgroundColor: 'white',
                height: '100%'
            }}>
             <View style={{
           flex: 1
             }}>
               <ScrollView  refreshControl={
                  <RefreshControl refreshing={loading} onRefresh={loadUserData} />
                }>
               {data.map((data, index) => {
                 return(
                  <View key={index} style={{
                    width:'94%',
                    marginRight:'3%',
                    marginLeft:'3%',
                 borderWidth: 0.2,
                 borderColor:'grey',
                        flexDirection:'row',
                        borderRadius:5,
                        marginTop:5
                        
                  }}>
                  <Avatar.Image style={{
                    margin:9
                  }} size={50} source={require('../Components/images/pic6.png')} />
                  <Text style={{
                   marginLeft:0,
                   marginRight:10,
                   marginTop:15,
                   width:'80%'
                  }}>{data.message}</Text>
                  </View>
   
                 )
               })}
              </ScrollView>
               
             </View>
                </View>
          
        
         </SafeAreaView>
       
    );

};
const style = StyleSheet.create({
    
  checkBoxStyle: {
    fontSize:15,
    fontWeight: 'bold',
    paddingLeft:5,
    marginTop: 7,
     marginLeft: 0
   },
   img: {
   backgroundColor: 'transparent',
   marginLeft: 'auto',
   marginRight: 'auto',
 	}
   
  });
  export default NotificationsDhobi;