import React, { useState } from 'react';
import {
  SafeAreaView,
  View,
  StatusBar,
  Text,
  TextInput,
  FlatList,
  Dimensions,
  StyleSheet,
  Image,
  Pressable,
  ScrollView,
  Button
} from 'react-native';
import Axios from 'axios'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialIcons';
import baseURL from './BaseURL/baseURL';
import { ActivityIndicator, Searchbar } from 'react-native-paper';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Map from './Map'
import DhobiSelection2 from './DhobiSelection2';

const Tab = createMaterialTopTabNavigator();

function TopTab() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Search by Search BAr" component={DhobiSelection2} />
      <Tab.Screen name="Search by Map" component={Map} />
    </Tab.Navigator>
  );
}

export default TopTab;