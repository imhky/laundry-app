import React, { useState } from 'react';
import {
  SafeAreaView,
  View,
  StatusBar,
  Text,
  FlatList,
  Dimensions,
  StyleSheet,
  Image,
  Pressable,
  ScrollView,
  Platform,
  ToastAndroid
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { setDhobiLoginDetails } from '../Redux/Slice';
import { Formik } from 'formik'
import * as yup from 'yup'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ServiceDetails from './ServiceDetails';
import {TextInput, Checkbox, Avatar, Button} from 'react-native-paper';
import baseURL from './BaseURL/baseURL';
import Axios from 'axios';
import { fonts } from 'react-native-elements/dist/config';
const createService = ({navigation}) => {
  const [data, setData]  = useState([1, 3, 4, 5])
  const [title, setTitle] = React.useState([]);
  const [description, setDescription] = React.useState([]);
  const [NormalPrice, setNormalPrice] = React.useState([0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
  const [UrgentPrice, setUrgentPrice] = React.useState([0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
  const [NormalNo, setNormalNo] = React.useState([0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
  const [UrgentNo, setUrgentNo] = React.useState([0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
  const [Checked, setChecked] = React.useState([false, false, false, false, false, false, false]);
  const [Location, setLocation] = React.useState([])
 
  const dispatch = useDispatch();
  const { loginDhobi } = useSelector((state) => state.loginDhobi);
  console.log('LOGIN DHOBI::::', loginDhobi)
  const dhobiID = loginDhobi._id
  
  const [isButtonDisabled, setIsButtonDisabled] = React.useState(false)
  const [orderFrequency, setOrderFrequency] = React.useState(loginDhobi.frequency_order)
  const [rows, setRows] = useState([
    {
      img: 'https://cdn-icons-png.flaticon.com/128/821/821528.png',
      title: 'Washed and Dried Laundry',
      description: 'Everyday laundry. Washed at 30 C, Tumble dried. Priced per load of 6kg',
      NormalPrice: '180',
      UrgentPrice: '120',
      Checked: false,
      Location: 'Mardan',
      NormalNo: '3',
      UrgentNo: '3'
    },
    {
      img: 'https://cdn-icons-png.flaticon.com/128/1127/1127580.png',
      title: 'Hi-Temp Wash and Dry',
      description: 'Washed laundry at more than 45 C, tumble dried. Priced per load of 6kg',
      NormalPrice: '170',
      UrgentPrice: '23',
      Checked: false,
      Location: 'Dir Lower',
      NormalNo: '3',
      UrgentNo: '3'
    },
    {
      img: 'https://cdn-icons-png.flaticon.com/128/2228/2228103.png',
      title: 'Washed and Ironed Laundry',
      description: 'Everyday laundry, ironed after driying and put on hangers. Priced per item.',
      NormalPrice: '170',
      UrgentPrice: '23',
      Checked: false,
      Location: 'Dir Lower',
      NormalNo: '3',
      UrgentNo: '3'
    },
    {
      img: 'https://cdn-icons-png.flaticon.com/128/3043/3043787.png',
      title: 'Dry Cleaning',
      description: 'Cleaned and ironed, put on hangers by default. Priced per item.',
      NormalPrice: '170',
      UrgentPrice: '23',
      Checked: false,
      Location: 'Dir Lower',
      NormalNo: '3',
      UrgentNo: '3'
    },
    {
      img: 'https://cdn-icons-png.flaticon.com/128/2228/2228103.png',
      title: 'Ironing only',
      description: 'You take care of washing, we just iron them for you. Priced per item.',
      NormalPrice: '170',
      UrgentPrice: '23',
      Checked: false,
      Location: 'Dir Lower',
      NormalNo: '3',
      UrgentNo: '3'
    }
  ])
  const createServicesValidationSchema = yup.object().shape({
    orderFrequency: yup
        .string()
        .matches(/\d/, "Please Enter only Number")
        .required('Order Frequency is required'),
  })
  console.log('rows:::::', rows)
  const handleChange1 = idx => e => {
    
    const {eventCount, target, text} = e.nativeEvent
    console.log('val::::::', text)
    title[idx] = text
    
    const rowss = [...rows]
    rowss[idx] = {
      title: text,
      // quantity: v,
      description: rows[idx].description,
      NormalPrice: rows[idx].NormalPrice,
      UrgentPrice: rows[idx].UrgentPrice,
      Checked:rows[idx].Checked,
      Location: rows[idx].Location,
      NormalNo: rows[idx].NormalNo === undefined ? NormalNo[idx] : rows[idx].NormalNo,
      UrgentNo: rows[idx].UrgentNo,
      img: rows[idx].img
     
    }
    setRows(rowss)
  }
  const handleChange2 = idx => e => {
    // 
     const {eventCount, target, text} = e.nativeEvent
    description[idx] = text
    
    const rowss = [...rows]
    rowss[idx] = {
      title: rows[idx].title,
      // quantity: v,
      description: text,
      NormalPrice: rows[idx].NormalPrice,
      UrgentPrice: rows[idx].UrgentPrice,
      Checked:rows[idx].Checked,
      Location: rows[idx].Location,
      NormalNo: rows[idx].NormalNo === undefined ? NormalNo[idx] : rows[idx].NormalNo,
      UrgentNo: rows[idx].UrgentNo,
      img: rows[idx].img
     
    }
    setRows(rowss)
  }

  const handleChange3 = idx => e => {
    // 
     const {eventCount, target, text} = e.nativeEvent
    NormalPrice[idx] = text
    
    const rowss = [...rows]
    rowss[idx] = {
      title: rows[idx].title,
      // quantity: v,
      description: rows[idx].description,
      NormalPrice: text,
      UrgentPrice: rows[idx].UrgentPrice,
      Checked:rows[idx].Checked,
      Location: rows[idx].Location,
      NormalNo: rows[idx].NormalNo === undefined ? NormalNo[idx] : rows[idx].NormalNo,
      UrgentNo: rows[idx].UrgentNo,
        img: rows[idx].img
     
    }
    setRows(rowss)
  }

  const handleChange4 = idx => e => {
    // 
     const {eventCount, target, text} = e.nativeEvent
    UrgentPrice[idx] = text
    
    const rowss = [...rows]
    rowss[idx] = {
      title: rows[idx].title,
      // quantity: v,
      description: rows[idx].description,
      NormalPrice: rows[idx].NormalPrice,
      UrgentPrice: text,
      Checked:rows[idx].Checked,
      Location: rows[idx].Location,
      NormalNo: rows[idx].NormalNo === undefined ? NormalNo[idx] : rows[idx].NormalNo,
      UrgentNo: rows[idx].UrgentNo,
        img: rows[idx].img
     
    }
    setRows(rowss)
  }

  const handleChange6 = idx => e => {
    // 
     const {eventCount, target, text} = e.nativeEvent
    Location[idx] = text
    
    const rowss = [...rows]
    rowss[idx] = {
      title: rows[idx].title,
      description: rows[idx].description,
      NormalPrice: rows[idx].NormalPrice,
      UrgentPrice: rows[idx].UrgentPrice,
      Checked:rows[idx].Checked,
      Location: text,
      NormalNo: rows[idx].NormalNo === undefined ? NormalNo[idx] : rows[idx].NormalNo,
      UrgentNo: rows[idx].UrgentNo,
        img: rows[idx].img
     
    }
    setRows(rowss)
  }

  const handleChange7 = idx => e => {
    // 
     const {eventCount, target, text} = e.nativeEvent
    NormalNo[idx] = text
    
    const rowss = [...rows]
    rowss[idx] = {
      title: rows[idx].title,
      description: rows[idx].description,
      NormalPrice: rows[idx].NormalPrice,
      UrgentPrice: rows[idx].UrgentPrice,
      Checked:rows[idx].Checked,
      Location: rows[idx].Location,
      NormalNo: text,
      UrgentNo: rows[idx].UrgentNo,
      img: rows[idx].img
     
    }
    setRows(rowss)
  }

  const handleChange8 = idx => e => {
    // 
     const {eventCount, target, text} = e.nativeEvent
    UrgentNo[idx] = text
    
    const rowss = [...rows]
    rowss[idx] = {
      title: rows[idx].title,
      description: rows[idx].description,
      NormalPrice: rows[idx].NormalPrice,
      UrgentPrice: rows[idx].UrgentPrice,
      Checked:rows[idx].Checked,
      Location: rows[idx].Location,
      NormalNo: rows[idx].NormalNo === undefined ? NormalNo[idx] : rows[idx].NormalNo,
      UrgentNo: text,
      img: rows[idx].img
     
    }
    setRows(rowss)
  }
  const handleChange5 = (idx) => {
    Checked[idx] = !Checked[idx]
    
    const rowss = [...rows]
    rowss[idx] = {
      title: rows[idx].title,
      description: rows[idx].description,
      NormalPrice: rows[idx].NormalPrice,
      UrgentPrice: rows[idx].UrgentPrice,
      Checked:  Checked[idx],
      Location: rows[idx].Location,
      NormalNo: rows[idx].NormalNo === undefined ? NormalNo[idx] : rows[idx].NormalNo,
      UrgentNo: rows[idx].UrgentNo,
      img: rows[idx].img
     
    }
    setRows(rowss)
  }
  console.log('Rows::::', rows)

  const updateOrderFrequency = () => {
    setIsButtonDisabled(true)
    const details = { frequency_order:orderFrequency };
    Axios.put(`${baseURL}/adminAuth/updateFrequencyOrder/${dhobiID}`, details)
    .then(res => {
    console.log('Updated OF:::::::', res.data)

              if (res.data.Result === "frequency updated") {

                dispatch(setDhobiLoginDetails({
                  Result: loginDhobi.Result,
                  __v: loginDhobi.__v,
                  _id: loginDhobi._id,
                  accessToken: loginDhobi.accessToken,
                  address: loginDhobi.address,
                  createdAt: loginDhobi.createdAt,
                  email:loginDhobi.email,
                  isService: true,
                  mobile_no: loginDhobi.mobile_no,
                  updatedAt: loginDhobi.updatedAt,
                  username: loginDhobi.username,
                  profilePic: loginDhobi.profilePic,
                  frequency_order: orderFrequency
                })); 
                ToastAndroid.showWithGravity(
                  `Order Frequency Set to ${orderFrequency}`,
                  ToastAndroid.SHORT,
                  ToastAndroid.CENTER
                );
              }
      
  }).catch(err => {
      console.log('ERROR :::: ', err)
  })
  setTimeout(() => {
    setIsButtonDisabled(false)
  }, 3000)
  }

    return(
    
       <SafeAreaView style=
       {{
         flex:1 , 
         backgroundColor:"white"
         
         }}>
             <View style={{
                 backgroundColor: '#0f9df7',
                 padding:10,
                 marginLeft:'5%',
                 marginRight:'5%',
                 marginTop:30,
                 marginBottom:'1%',
                 borderRadius: 20
                
             }}>
                 <Text style={{
                      textAlign:'center',
                     color: 'white',
                     fontWeight:'bold',
                     fontSize: 20
                 }}>Choose & Create your Service!</Text>
             </View>
             <ScrollView>
               <View style={{
                   flexDirection:"row",
                   justifyContent:"space-between"
                  //  alignItems:"center"
               }}>
               <TextInput
                     label="Order Frequency"
                     mode="outlined"
                     style={{marginTop: '3%', marginLeft:'3%', marginRight:'3%', width:'94%'}}
                     value={orderFrequency}
                     onChangeText={text => setOrderFrequency(text)}
                     placeholder='Enter Order Per Day'
               />
               {/* <Button disabled={isButtonDisabled} mode="outlined" color="blue" onPress={() => updateOrderFrequency()}
                
                style={{
                    borderColor: 'blue',
                    top:'2%',
                    margin:'3%',
                    width:'27%',
                    // marginLeft:'6%',
                    // marginRight:'6%',
                    padding:5
                }} >
                 SAVE
                </Button> */}
            
               </View>
             <Formik
   validationSchema={createServicesValidationSchema}
   initialValues={{orderFrequency: '3'}}
   onSubmit={values => {
    console.log('Values::::::', values)
    setIsButtonDisabled(true)
    console.log('Rows::INSIDE::', rows)
    fetch(`${baseURL}/serviceManipulate/post`, {
      method:"POST",
      headers:{
          "Content-Type":"application/json"
      },
      body:JSON.stringify({
              admin_id:dhobiID,
              // frequency_order:values.orderFrequency,
              Services:rows
      })
  }).then(res => res.json())
  .then(res => {
    console.log('CREATE SERVICE RES:::::::', res)
      if (res.Result === "your services has been created successfully") {
          console.log(res)
               ToastAndroid.showWithGravity(
                "Your Services have been created successfully!",
                ToastAndroid.SHORT,
                ToastAndroid.CENTER
              );
            
              Axios.get(`${baseURL}/serviceManipulate/setIsService/${dhobiID}`)
              .then(res => {
                console.log('IsServcie:::Res:::::', res)
                dispatch(setDhobiLoginDetails({
                  Result: loginDhobi.Result,
                  __v: loginDhobi.__v,
                  _id: loginDhobi._id,
                  accessToken: loginDhobi.accessToken,
                  address: loginDhobi.address,
                  createdAt: loginDhobi.createdAt,
                  email:loginDhobi.email,
                  isService: true,
                  mobile_no: loginDhobi.mobile_no,
                  updatedAt: loginDhobi.updatedAt,
                  username: loginDhobi.username,
                })); 
                const resp = JSON.stringify(res.data)
                navigation.navigate('NavTabCustomer')
                console.log('IsServcie:::Res:::::', resp)

              })
              .catch(err => console.log(err))
          
      } else if (res.code === '11000') {
    
             ToastAndroid.showWithGravity(
              "Lets Navigate to Dashboard!",
              ToastAndroid.SHORT,
              ToastAndroid.CENTER
            );
            navigation.navigate('Dashboard')
    }   else  {
        ToastAndroid.showWithGravity(
          "Somthing Went Wrong!",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
        // navigation.navigate('Dashboard')
      }
   
  }).catch(err => {
      console.log('ERROR :::: ', err)
  })
  setTimeout(() => {
    console.log('CALLED ::::')
    setIsButtonDisabled(false)
   
  }, 3000)
   }}
 >
  {({
     handleChange,
     handleBlur,
     handleSubmit,
     values,
     errors,
     isValid,
   }) => (
     <>
      <View >
         
                </View>
                 {rows.map((data, idx) => {
                     return(<View key={idx} style={{
                        margin:'2%',
                        borderWidth:1,
                        padding:'2%',
                        borderRadius: 10,
                        borderWidth:1,
                        borderColor:'grey'
                      }}>
          
           <Image source={{uri: `${rows[idx].img}`}}
                style={style.image} />
                      <TextInput
                     label="Title"
                     mode="outlined"
                     value={rows[idx].title}
                     
                    //  onChangeText={title => setTitle(title)}
                    onChange={handleChange1(idx)}
               />
           
                     <TextInput
                     label="Description"
                     mode="outlined"

                     multiline={true}
                     numberOfLines={4}
                     value={rows[idx].description}
                     onChange={handleChange2(idx)}
               />
                   <TextInput
                     label="Area to Cover (Location)"
                     mode="outlined"
                     style={{marginTop: 2, marginLeft:2, marginRight:2}}
                     value={rows[idx].Location}
                     onChange={handleChange6(idx)}
               />
                <View style={{flexDirection: 'row', marginTop: 4}}>
                <TextInput
                     label="Normal Delivery Price (PKR)"
                     mode="outlined"
                     style={{width:'49%', marginTop: 2, marginLeft:2, marginRight:2}}
                     value={rows[idx].NormalPrice}
                     onChange={handleChange3(idx)}
               />
                   <TextInput
                     label="Urgent Delivery Price (PKR)"
                     mode="outlined"
                     style={{width:'49%', marginTop: 2, marginLeft:2, marginRight:2}}
                     value={rows[idx].UrgentPrice}
                     onChange={handleChange4(idx)}
               />
                </View>
                <View style={{flexDirection: 'row', marginTop: 4}}>
                <TextInput
                     label="Normal Delivery / Day"
                     mode="outlined"
                     style={{width:'49%', marginTop: 2, marginLeft:2, marginRight:2}}
                     value={rows[idx].NormalNo === undefined ? NormalNo[idx] : rows[idx].NormalNo}
                     onChange={handleChange7(idx)}
               />
                   <TextInput
                     label="Urgent Delivery / Day"
                     mode="outlined"
                     style={{width:'49%', marginTop: 2, marginLeft:2, marginRight:2}}
                     value={rows[idx].UrgentNo}
                     onChange={handleChange8(idx)}
               />
                </View>
            
              
            
                 <View style={{flexDirection: 'row', marginTop: 10, marginLeft: 3}}>
              
                        <Checkbox
                 status={Checked[idx] ? 'checked' : 'unChecked'}
                 onPress={() => {
                  handleChange5(idx)
                 }}
                 color="#0f9df7"
               />
                <Text style={style.checkBoxStyle}>Include this service?</Text>
                
                 </View>
              
                      </View>

                     )
                 })}

<Button mode="outlined" color="rgba(9, 135, 227, 0.74)" onPress={() => {
    handleSubmit()
    updateOrderFrequency()
}}  disabled={isButtonDisabled}
                
                style={{
                    borderColor: 'green',
                    margin:10,
                    marginTop:'1%'
                }} >
                 Save Services
                </Button>
                </>
   )}
 </Formik>
           </ScrollView>
       
          
        
         </SafeAreaView>
       
    );

};
const style = StyleSheet.create({
    
  checkBoxStyle: {
    fontSize:15,
    fontWeight: 'bold',
    paddingLeft:5,
    marginTop: 7,
     marginLeft: 0
   },
       image:{
        backgroundColor: 'transparent',
        marginLeft: 'auto',
        marginRight: 'auto',
        borderWidth: 1,
        borderRadius:20,
        borderColor: 'grey',
        height: 100,
        width:100
       }
   
  });
  export default createService;