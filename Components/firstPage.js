import * as React from 'react';
import {SafeAreaView, View, StyleSheet, TextInput , Pressable, Text  } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import PushNotificationTest from '../Components/pushNotificationTest'

const FirstPage = ({navigation}) => {


    return(
        <SafeAreaView style={{
            backgroundColor: 'white',
           
            height: '100%'
        }}>
   
             <View style={{
                 width: '100%',
                 backgroundColor: '#0987E3',
                 height: 130,
                 borderTopEndRadius: 100,
                 marginBottom:'3%'
             }}>
                 <Text style={{
                     color: 'white',
                     fontFamily:'sans-serif',
                     fontWeight:'bold',
                     fontSize: 38,
                     marginTop: '8%',
                     marginLeft:'5%'
                 }}>Welcome to</Text>
                   <Text style={{
                     color: 'white',
                     fontFamily:'sans-serif',
                     fontSize: 22,
                     marginLeft:'6%'
                 }}>Dhobi Ghat & Laundry!</Text>

             </View>
          
             <Text style={{
                     color: '#8B6060',
                     fontFamily:'sans-serif',
                     fontSize: 22,
                     marginLeft:'8%',
                     marginTop:'3%',
                     marginRight: '8%'
                 }}>Best Wash & Iron service
                 at your doorstep!</Text>

                 <Text style={{
                     color: '#816464',
                     fontFamily:'sans-serif',
                     fontSize: 15,
                     marginLeft:'8%',
                     marginTop:'3%',
                     marginRight: '8%'
                 }}>          Dhobi Ghat & Laundry is an Online Laundry Platform in Peshawar, Pakistan with the latest technology in washing, dry cleaning, ironing and laundry. Our services combine our expertise and experience acquired over a period of time to provide you with clean laundry in the shortest possible 
                 turnaround time.</Text>

                 <TouchableOpacity onPress={()=>navigation.navigate('Login')}>
                    <View style={{
                        marginTop:'8%',
                        marginBottom: '3%',
                        height: 38,
                        marginHorizontal: 20,
                        borderWidth: 1,
                    borderColor: "#0f9df7",
                    borderRadius:50,
                
                    backgroundColor: '#0f9df7',
                        width:'89%',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        <Text style={{fontSize: 18, color: 'white'}}>Login as User</Text>
                    </View>
                    
                    </TouchableOpacity>

                    <Text style={{
                     color: '#0f9df7',
                     fontFamily:'sans-serif',
                     fontSize: 40,
                     justifyContent: 'center',
                   
                     textAlign: 'center',
                     fontWeight: 'bold'
                 }}>OR</Text>

                    <TouchableOpacity onPress={()=>navigation.navigate('LoginDhobi')}>
                    <View style={{
                        marginTop:'3%',
                        marginBottom: '3%',
                        height: 38,
                        marginHorizontal: 20,
                        borderWidth: 1,
                    borderColor: "#0f9df7",
                    borderRadius:50,
                
                    backgroundColor: '#0f9df7',
                        width:'89%',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        <Text style={{fontSize: 18, color: 'white'}}>Login as Dhobi</Text>
                    </View>
                    
                    </TouchableOpacity>
                    <PushNotificationTest />
        </SafeAreaView>
       
    )
}

export default FirstPage;