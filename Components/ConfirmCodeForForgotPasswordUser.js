/*
Concept: https://dribbble.com/shots/5476562-Forgot-Password-Verification/attachments
*/
import {Animated, Image, Pressable, SafeAreaView, Text, ToastAndroid, View} from 'react-native';
import React, {useState} from 'react';

import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';

import styles, {
  ACTIVE_CELL_BG_COLOR,
  CELL_BORDER_RADIUS,
  CELL_SIZE,
  DEFAULT_CELL_BG_COLOR,
  NOT_EMPTY_CELL_BG_COLOR,
} from './styles';
import baseURL from './BaseURL/baseURL';

const {Value, Text: AnimatedText} = Animated;

const CELL_COUNT = 4;
const source = {
  uri: 'https://cdn-icons-png.flaticon.com/512/891/891399.png',
};

const animationsColor = [...new Array(CELL_COUNT)].map(() => new Value(0));
const animationsScale = [...new Array(CELL_COUNT)].map(() => new Value(1));
const animateCell = ({hasValue, index, isFocused}) => {
  Animated.parallel([
    Animated.timing(animationsColor[index], {
      useNativeDriver: false,
      toValue: isFocused ? 1 : 0,
      duration: 250,
    }),
    Animated.spring(animationsScale[index], {
      useNativeDriver: false,
      toValue: hasValue ? 0 : 1,
      duration: hasValue ? 300 : 250,
    }),
  ]).start();
};

const AnimatedExample = ({route, navigation}) => {
    const {  
        email
      } = route.params;
  const [value, setValue] = useState('');
  const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });
  const [isButtonDisabled, setIsButtonDisabled] = React.useState(false)
  const [loading, setLoading] = React.useState(false)

  console.log('value:::::', value)
  const renderCell = ({index, symbol, isFocused}) => {
    const hasValue = Boolean(symbol);
    const animatedCellStyle = {
      backgroundColor: hasValue
        ? animationsScale[index].interpolate({
            inputRange: [0, 1],
            outputRange: [NOT_EMPTY_CELL_BG_COLOR, ACTIVE_CELL_BG_COLOR],
          })
        : animationsColor[index].interpolate({
            inputRange: [0, 1],
            outputRange: [DEFAULT_CELL_BG_COLOR, ACTIVE_CELL_BG_COLOR],
          }),
      borderRadius: animationsScale[index].interpolate({
        inputRange: [0, 1],
        outputRange: [CELL_SIZE, CELL_BORDER_RADIUS],
      }),
      transform: [
        {
          scale: animationsScale[index].interpolate({
            inputRange: [0, 1],
            outputRange: [0.2, 1],
          }),
        },
      ],
    };

    // Run animation on next event loop tik
    // Because we need first return new style prop and then animate this value
    setTimeout(() => {
      animateCell({hasValue, index, isFocused});
    }, 0);


    return (
      <AnimatedText
        key={index}
        style={[styles.cell, animatedCellStyle]}
        onLayout={getCellOnLayoutHandler(index)}>
        {symbol || (isFocused ? <Cursor /> : null)}
      </AnimatedText>
    );
  };
  const postRegisterData = () => {
    setLoading(true)
    console.log('Values:Inside:::::', email)
    fetch(`${baseURL}/resetEmail/verifyUser`, {
        method:"POST",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
                randomNumber: Number(value)
                // password: password,
                // username: username,
                // mobile_no: mobile_no,
                // address:  address

        })
    }).then(res => res.json())
    .then(res => {
      console.log('REGISTER RES:::::::', res)
        if (res.Result === "user verification successed") {
            console.log(res)
                 ToastAndroid.showWithGravity(
                  "Please Enter your New Password now!",
                  ToastAndroid.SHORT,
                  ToastAndroid.CENTER
                );
                navigation.replace('newPasswordForUserField', {
                  email
                })
                setLoading(false)
        }  else if (res.Result === "User already exist in this email") {
          console.log(res)
               ToastAndroid.showWithGravity(
                "User already exists with this email!",
                ToastAndroid.SHORT,
                ToastAndroid.CENTER
              );
               setLoading(false)
      }  else if (res.message === "You enter invalid verification code") {
        console.log(res)
             ToastAndroid.showWithGravity(
              "You entered invalid verification code!",
              ToastAndroid.SHORT,
              ToastAndroid.CENTER
            );
            setValue('')
             setLoading(false)
             } else  {
          ToastAndroid.showWithGravity(
            "Somthing Went Wrong!",
            ToastAndroid.SHORT,
            ToastAndroid.CENTER
          );
           setLoading(false)
        }
     
    }).catch(err => {
        console.log('ERROR :::: ', err)
    })
   
    setTimeout(() => {
      setIsButtonDisabled(false)
    }, 3000)
}
  return (
    <SafeAreaView style={styles.root}>
      <Text style={styles.title}>Verification</Text>
      <Image style={styles.icon} source={source} />
      <Text style={styles.subTitle}>
        Please enter the verification code{'\n'}
        we sent to your email address!
      </Text>

      <CodeField
        ref={ref}
        {...props}
        value={value}
        onChangeText={setValue}
        cellCount={CELL_COUNT}
        rootStyle={styles.codeFieldRoot}
        keyboardType="number-pad"
        textContentType="oneTimeCode"
        renderCell={renderCell}
      />
      <Pressable disabled={isButtonDisabled}>
      <View style={styles.nextButton} >
        <Text onPress={() => {
          
          postRegisterData()

      }} style={styles.nextButtonText}>Verify</Text>
      </View>
      </Pressable>
    </SafeAreaView>
  );
};

export default AnimatedExample;