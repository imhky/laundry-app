import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  View,
  StatusBar,
  Text,
  TextInput,
  FlatList,
  Dimensions,
  StyleSheet,
  Image,
  Pressable,
  ScrollView,
  LogBox,
  ToastAndroid
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { setLoginDetails } from '../Redux/Slice';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Button } from 'react-native-paper';
import baseURL from './BaseURL/baseURL';
import Axios from 'axios';
import * as ImagePicker from 'expo-image-picker';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Ionicons from '@expo/vector-icons/Ionicons';

import { getApps, initializeApp } from "firebase/app";
import { getStorage, ref, uploadBytes, getDownloadURL } from "firebase/storage";
import uuid from "uuid";
import axios from 'axios';

const firebaseConfig = {
    apiKey: "AIzaSyC5QHD_X7RO3RzOZzK_PsYYmVBu6QAF9nA",
    authDomain: "laundryapp-15598.firebaseapp.com",
    projectId: "laundryapp-15598",
    storageBucket: "laundryapp-15598.appspot.com",
    messagingSenderId: "359858181338",
    appId: "1:359858181338:web:37bac919dcf0f65c7fc1e3",
    measurementId: "G-2LY7XDDQ7V"
  };
  
  // Editing this file with fast refresh will reinitialize the app on every refresh, let's not do that
  if (!getApps().length) {
    initializeApp(firebaseConfig);
  }
  
  // Firebase sets some timeers for a long period, which will trigger some warnings. Let's turn that off for this example
  LogBox.ignoreLogs([`Setting a timer for a long period`]);

const UserProfile = ({navigation}) =>{
    const dispatch = useDispatch();
  const { login} = useSelector((state) => state.login);
  const [totalOrder, setTotalOrder] = React.useState(0)
  const [processingOrder, setProcessingOrder] = React.useState(0)
  const [cancelledOrder, setCancelledOrder] = React.useState(0)
  const [image, setImage] = React.useState(login.profilePic);
  const [uploading, setUplaoding] = useState(false)


  React.useEffect(() => {
    Axios.get(`${baseURL}/imageUpload/photo/${login._id}`).then(res => {
        console.log('get Profile::::::', res.data)
        setImage(res.data.profilePic)
      }).catch(err => console.log(err))

    Axios.get(`${baseURL}/orderManipulate/noOfOrder/${login._id}`).then(res => {
      console.log('Orders::::::', res.data)
      setTotalOrder(res.data)
    }).catch(err => console.log(err))

    Axios.get(`${baseURL}/orderManipulate/processingOrder/${login._id}`).then(res => {
        console.log('P Orders::::::', res.data)
        setProcessingOrder(res.data)
      }).catch(err => console.log(err))

      Axios.get(`${baseURL}/orderManipulate/cancelledOrder/${login._id}`).then(res => {
        console.log('C Orders::::::', res.data)
        setCancelledOrder(res.data)
      }).catch(err => console.log(err))
   },[])

const uploadImageToNodeJS = (url) => {
    Axios.put(`${baseURL}/imageUpload/uploadPhoto/${login._id}`, {
      profileImage: url
    })
   .then(res => {
   console.log('Image uploaded :::::::', res.data)

             if (res.data.result === "Profile Uploaded successfully!") {

               ToastAndroid.showWithGravity(
                 `Profile Uploaded successfully!`,
                 ToastAndroid.SHORT,
                 ToastAndroid.CENTER 
               );
             } else {
                ToastAndroid.showWithGravity(
                    `Profile did not Upload, Please try again`,
                    ToastAndroid.SHORT,
                    ToastAndroid.CENTER
                  );
             }
     
 }).catch(err => {
     console.log('ERROR :::: ', err)
 })
   

}
   
   
  const handleImagePicked = async (pickerResult) => {
    try {
      setUplaoding(true)

      if (!pickerResult.cancelled) {
        ToastAndroid.showWithGravity(
          `Profile is being uploading!`,
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
        const uploadUrl = await uploadImageAsync(pickerResult.uri);
       console.log('Imaged Upload, url is:', uploadUrl)
       setImage(uploadUrl)
       uploadImageToNodeJS(uploadUrl)
      }
    } catch (e) {
      console.log(e);
      alert("Upload failed, sorry :(");
    } finally {
        setUplaoding(false)
    }
  };
  
  const pickImage = async () => {
    // No permissions request is necessary for launching the image library
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 4],
      quality: 1,
    });

    console.log('Complete Result:::',result);

    console.log(result);
    handleImagePicked(result);
    // if (!result.cancelled) {
    //   setImage(result.uri);
    // }
  };

    //Axios

//     const formData = new FormData();
//     formData.append("file", result);
//     Axios.put(`${baseURL}/imageUpload/postUser/6182c88cfbc7fa6a18da5f0d`, formData)
//     .then(res => {
//     console.log('Image uploaded :::::::', res)

//               if (res.data.Result === "frequency updated") {

//                 ToastAndroid.showWithGravity(
//                   `Order Frequency Updated to ${orderFrequency}`,
//                   ToastAndroid.SHORT,
//                   ToastAndroid.CENTER
//                 );
//               }
      
//   }).catch(err => {
//       console.log('ERROR :::: ', err)
//   })

    return(
        <SafeAreaView style={{
            flex:1,
            backgroundColor:"white"
        }}>
            
          <View style={{
            width:"100%",
           // height: 170,
           height:120,
            backgroundColor: "rgba(9, 135, 227, 0.74)"
        }}>
            {// Inner portion one Starts
            }
          <View style={{
               flexDirection:"column",
               justifyContent:"space-between",
               alignItems:"center"
           }}>
         <Button mode="outlined"  labelStyle={{ color: "white", fontSize: 12 }} color="rgba(9, 135, 227, 0.74)" 
                style={{
                  width: 98,
                  height: 28,
                  left:'33%',
                   top: 8,
                   backgroundColor:"rgba(1, 122, 209, 0.75)",
                   marginHorizontal: 20,
                   borderWidth: 0.5,
                   borderColor:"rgba(1, 122, 209, 0.75)",
               borderRadius:50,
                   justifyContent: 'center',
                   alignItems: 'center',
                }} >
                 Support
                </Button>
      
        <Button mode="outlined"  labelStyle={{ color: "white", fontSize: 12 }} color="rgba(9, 135, 227, 0.74)"  onPress={() => {
            dispatch(setLoginDetails({}));
            navigation.navigate('FirstPage')
          }}
                
                style={{
                 
                  width: 98,
                  height: 28,
                left:'33%',
                 top: 15,
                 shadowColor:'red',
                 backgroundColor:"rgba(254, 8, 8, 0.75)",
                 marginHorizontal: 20,
                 borderWidth: 0.5,
             borderRadius:50,
             borderColor:"#E31A1A",
                 justifyContent: 'center',
                 alignItems: 'center',
                 color:'white'
                }} >
                 Logout
                </Button>
           
    
        <View style={{
             
               justifyContent:"space-between",
               alignItems:"center"
           }}>
        
               <Text style={{
                   width: 218,
                   height: 29,
                   top:27,
                   left:'20%',
                   fontStyle: "normal",
                   fontWeight: "normal",
                   fontSize: 16,
                   lineHeight: 19,
                   color: "#FFFFFF"
                   
               }}>{login.username}</Text>
           </View>
           </View>
           {
               //Inner portion one ends
           }
           {
               //Inner portion 2nd starts 
           }
           <View  style={{
               flexDirection:"row",
             
               alignItems:"center"
           }}>
               
           {(image === null || image === '' || image === undefined) ? <Image source={require("../Components/images/pic6.png")}
               style={{
                width: 100,
                height: 100,
                left: 23,
                top: -25,
               }} /> : <Image source={{ uri: image }}
              
               style={{
                width: 100,
                height: 100,
                left: 23,
                top: -25,
                borderRadius:50
               }} /> }
           <TouchableOpacity onPress={pickImage}><Ionicons style={{
               borderWidth:0.2,
               borderRadius:20,
               backgroundColor: 'white',
               borderColor:'rgba(9, 135, 227, 0.74)',
               padding:3
           }} name={'camera'} size={20} color='rgba(9, 135, 227, 0.74)' /></TouchableOpacity> 
         
           </View>
           {
               //Inner portion 2nd Ends
           }
           {
               //Inner portion 3rd Starts 
           }
         
           {
               //Inner portion 3rd Ends 
           }
        </View>
        
        <View>
            {
                //Inner 1st Portion Starts
            }
            <View style={{
                flexDirection:"row",
                alignItems:"center",
               
            }}>
              <Text style={{
                  width: 108,
                  height: 45,
                  left: "110%",
                  top:50,
                  fontStyle: "normal",
                  fontWeight: "bold",
                  fontSize: 20,
                  lineHeight: 19,
                  color: "rgba(1, 122, 209, 0.75)"
              }}>{totalOrder}</Text>
             <Text style={{
                  width: 108,
                  height: 45,
                  left: "160%",
                  top:50,
                  fontStyle: "normal",
                  fontWeight: "bold",
                  fontSize: 20,
                  lineHeight: 19,
                  color: "rgba(1, 122, 209, 0.75)"
              }}>{processingOrder}</Text>
              <Text style={{
                  width: 108,
                  height: 45,
                  left: "220%",
                  top: 50,
                  fontStyle: "normal",
                  fontWeight: "bold",
                  fontSize: 20,
                  lineHeight: 19,
                  color: "rgba(1, 122, 209, 0.75)"
              }}>{cancelledOrder}</Text>
            </View>
            {
                //1st Inner portion done
            }
            {
                //2nd Inner Portion Starts
            }
            <View style={{
                width:'90%',
                height:45,
                left:'20%',
            
                flexDirection:"row",
                alignItems:"center",
                borderBottomWidth: 1,
                borderBottomColor: 'rgba(1, 122, 209, 0.75)',
            }}>
                <Text style={{
                      width: 118,
                      height: 45,
                      left: '10%',
                      top: 20,
                      fontStyle: "normal",
                      fontWeight: "normal",
                      fontSize: 14,
                      lineHeight: 19,
                      color: "#000000"
                }}>Total Orders
                </Text>
                <Text style={{
                  width: 118,
                  height: 45,
                  left: '50%',
                  top:20,
                  fontStyle: "normal",
                  fontWeight: "normal",
                  fontSize: 14,
                  lineHeight: 19,
                  color: "#000000"
              }}>Inprogress</Text>
              <Text style={{
                  width: 118,
                  height: 45,
                  left: '90%',
                  top:20,
                  fontStyle: "normal",
                  fontWeight: "normal",
                  fontSize: 14,
                  lineHeight: 19,
                  color:"#000000"
              }}>Cancelled</Text>
            </View>
            {
                //2nd Inner Portion Ends
            }
        </View>
        {
            //3rd parent portion starts
        }
        <View style={{
            flexDirection:"row",
        }}>
             <Image source={require("../Components/images/pic8.png")}
               style={{
                width: 30,
                height: 20,
                left: 23,
                top: 25,
               }} />
               <Text style={{
                   width: 218,
                   height: 29,
                   left: 33,
                   top: 25,
                   fontStyle: "normal",
                   fontWeight: "normal",
                   fontSize: 15,
                   lineHeight: 19,                  
                   color: "#846262"
               }}>{login.email}</Text>
        </View>
        {
            //3rd Parent portion ends
        }
        {
            //4rth Parent portion starts
        }
        <View style={{
            flexDirection:"row",
        }}>
             <Image source={require("../Components/images/pic9.png")}
               style={{
                width: 30,
                height: 30,
                left: 20,
                top: 30,
               }} />
               <Text style={{
                   width: 218,
                   height: 29,
                   left: 28,
                   top: 35,
                   fontStyle: "normal",
                   fontWeight: "normal",
                   fontSize: 15,
                   lineHeight: 19,                  
                   color: "#846262"
               }}>+92 {login.mobile_no}</Text>
        </View>
        {
            //4rth Portion ends
        }
        {
            //5th portion starts
        }
        {/* <View style={{
            flexDirection:"row",
        }}>
             <Image source={require("../Components/images/pic10.png")}
               style={{
                width: 35,
                height: 48,
                left: 20,
                top: 40,
               }} />
               <Text style={{
                   width: 218,
                   height: 40,
                   left: 28,
                   top:45,
                   fontStyle: "normal",
                   fontWeight: "normal",
                   fontSize: 13,
                   lineHeight: 19,                  
                   color: "#846262"
               }}>Mohallah Burh hujjra, Cheena
               Rustam, Mardan, KPK.</Text>
        </View> */}
        {
            //5th Portion ends
        }
        </SafeAreaView>
        
    )
}
export default UserProfile;

async function uploadImageAsync(uri) {
    // Why are we using XMLHttpRequest? See:
    // https://github.com/expo/expo/issues/2402#issuecomment-443726662
    const blob = await new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function () {
        resolve(xhr.response);
      };
      xhr.onerror = function (e) {
        console.log(e);
        reject(new TypeError("Network request failed"));
      };
      xhr.responseType = "blob";
      xhr.open("GET", uri, true);
      xhr.send(null);
    });
  
    const fileRef = ref(getStorage(), uuid.v4());
    const result = await uploadBytes(fileRef, blob);
  
    console.log('RESULT::::::::::', result)
    // We're done with the blob, close and release it
    blob.close();
  
    return await getDownloadURL(fileRef);
  }
  