import * as React from 'react';
import {View, StyleSheet, TextInput, Pressable,ToastAndroid, BackHandler} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Text } from 'react-native-elements';
import { CheckBox } from 'react-native-elements/dist/checkbox/CheckBox';
import CheckBoxIcon from 'react-native-elements/dist/checkbox/CheckBoxIcon';
import { Input } from 'react-native-elements/dist/input/Input';
// import { Button } from 'react-native-elements';
import { Button } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';

import { Caption, Checkbox, ActivityIndicator } from 'react-native-paper';
import Services from './Services';
import { Formik } from 'formik'
import * as yup from 'yup'
// import { Icon } from 'native-base';
import baseURL from './BaseURL/baseURL';
import { useDispatch, useSelector } from 'react-redux';
import { setLoginDetails, setMessage } from '../Redux/Slice';

export default function Login({navigation}) {
  const [checked, setChecked] = React.useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = React.useState(false)
  const [loading, setLoading] = React.useState(true)
//   const onBackButtonPressAndroid = () => {
//     return (true);
// }
//   BackHandler.addEventListener('hardwareBackPress', onBackButtonPressAndroid);

    const dispatch = useDispatch();
    const { login } = useSelector((state) => state.login);
   

    React.useEffect(() => {
    
      if (login.Result === 'Login success') {
        navigation.replace('TabNav')
        setLoading(false)
      } else {
        setLoading(false)
      }
      
    

    },[])

    const [iconNpass, setIconNpass] = React.useState({
      icon: "eye-off",
      passwordSecure: true
    })

    const changeIcon = () => {
      const IC = iconNpass.icon === 'eye' ? 'eye-off' : 'eye'
      const P = !iconNpass.passwordSecure
      setIconNpass({
        icon:IC,
        passwordSecure: P
      })
    }
console.log('LOGIN:::::', login)

  const loginValidationSchema = yup.object().shape({
    email: yup
      .string()
      .email("Please enter valid email")
      .required('Email Address is Required'),
    password: yup
        .string()
        .matches(/\w*[a-z]\w*/,  "Password must have a small letter")
        // .matches(/\w*[A-Z]\w*/,  "Password must have a capital letter")
        .matches(/\d/, "Password must have a number")
        // .matches(/[!@#$%^&*()\-_"=+{}; :,<.>]/, "Password must have a special character")
        .min(8, ({ min }) => `Password must be at least ${min} characters`)
        .required('Password is required'),
  })

  return (
   <View style={styles.container}>
     
             
         
     {loading ? (<ActivityIndicator size="large" color="#0f9df7" style={{top:'50%'}} />) : (<>
      <View style={{
        flexDirection: 'row'
      }}>
      
      <View>
      <View style={{
                 width: 200,
                 backgroundColor: '#0987E3',
                 height: 200,
                 opacity:0.8,
                borderRadius:100,
                marginLeft: -60,
                marginTop: -85
               
             }}>
            </View>
            <View style={{
                marginTop: -160,
                 width: 200,
                 backgroundColor: '#0987E3',
                 height: 200,
              opacity:0.8,
                borderRadius:100,
                marginLeft: -90
               
             }}>
            </View>
      </View>
      <Pressable>
      <View>
      <Text style={styles.text1}>Sign in</Text>
      </View>
      </Pressable>
       </View>
       <Formik
   validationSchema={loginValidationSchema}
   initialValues={{ email: '', password: '' }}
   onSubmit={values => {
    setLoading(true)
    console.log('Values::::::', values)
    setIsButtonDisabled(true)
    fetch(`${baseURL}/userAuth/login`, {
      method:"POST",
      headers:{
          "Content-Type":"application/json"
      },
      body:JSON.stringify({
              email:values.email,
              password:values.password
             
      })
  }).then(res => res.json())
  .then(res => {
    console.log('LOGIN RES:::::::', res)
  
      if (res.Result === "Login success") {
          console.log(res)
          dispatch(setLoginDetails(res));
               ToastAndroid.showWithGravity(
                "You are Logged in Successfully!",
                ToastAndroid.SHORT,
                ToastAndroid.CENTER
              );
              navigation.replace('TabNav')
              setLoading(false)
      }  else if (res === "Wrong password or username!") {
        console.log(res)
             ToastAndroid.showWithGravity(
              "Wrong Password or Username!",
              ToastAndroid.SHORT,
              ToastAndroid.CENTER
            );
            setLoading(false)
            // navigation.navigate('TabNav')
    }else  {
        ToastAndroid.showWithGravity(
          "Con not connect to the Server!",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
        setLoading(false)
        // navigation.navigate('TabNav')
      }
   
  }).catch(err => {
      console.log('ERROR :::: ', err)
  })
  setTimeout(() => {
    setIsButtonDisabled(false)
  }, 3000)
   }}
 >
   {({
     handleChange,
     handleBlur,
     handleSubmit,
     values,
     errors,
     isValid,
   }) => (
     <>
      <View >
      <TextInput
          name="email"
          placeholder="Email Address"
        style={styles.field1}
        onChangeText={handleChange('email')}
        onBlur={handleBlur('email')}
        value={values.email}
        keyboardType="email-address"
        // placeholder='923440110318'
        underlineColorAndroid ='transparent'
        />
           {errors.email &&
         <Text style={{ fontSize: 10, color: 'red', marginLeft:'10%' }}>{errors.email}</Text>
       }
      
      </View>
     <View style={{
       flexDirection:'row'
     }}>
      <TextInput
       name="password"
    //    placeholder="Password"
        style={styles.field2}
        onChangeText={handleChange('password')}
        onBlur={handleBlur('password')}
        value={values.password}
        secureTextEntry={iconNpass.passwordSecure}
        width='90%'
        placeholder='Password'
        underlineColorAndroid ='transparent'
        />
        
          <Icon name={iconNpass.icon} 
          onPress={() => changeIcon()}
           size={25} style={{
             marginLeft: '-20%',
             top:18,
          }} color="#0987E3" />
      
      </View>
      {errors.password &&
         <Text style={{ fontSize: 10, color: 'red', marginLeft:'10%' }}>{errors.password}</Text>
       }
   <View style={{flexDirection: 'row'}}>
   <View style={{flexDirection: 'row', marginTop: 4, marginLeft: 30}}>
      {/* <CheckBoxIcon
          style={{color: 'red'}}
            title='Click Here'
            checked='checked'
          
            /> */}
             <Checkbox
      status={checked ? 'checked' : 'unchecked'}
      onPress={() => {
        setChecked(!checked);
      }}
      color="#0f9df7"
    />
     
      </View>
     <View>
     <Text style={styles.rememberme}> Remember me</Text>
     </View>
     <View style={{marginTop: 10, marginRight: 30}}>
         <TouchableOpacity onPress={() => {
            navigation.navigate('forgotPasswordEmailForUser')
         }}>
         <Text style={styles.forget}>Forget Password?</Text>
         </TouchableOpacity>
          
      </View>
   </View>
 
     <View style={{marginTop: 40}}>
 
      
     {/* <TouchableOpacity onPress={()=>navigation.navigate('TabNav')}> */}
     <TouchableOpacity onPress={handleSubmit}  disabled={!isValid  || isButtonDisabled}>
        <View style={{
            height: 38,
            marginHorizontal: 20,
            borderWidth: 1,
            borderColor: (!isValid || isButtonDisabled === true ? '#c4c4c4' : '#0f9df7'),
            borderRadius:50,
           
            backgroundColor: (!isValid || isButtonDisabled === true ? '#c4c4c4' : '#0f9df7'),
            width:'89%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            <Text style={{fontSize: 18, color: 'white'}}>Login</Text>
          </View>
        
        </TouchableOpacity>
     </View>
     </>
   )}
 </Formik>
     {/* <Button title="Show Toast" onPress={showToast} /> */}
     <View
        style={{
          borderBottomColor: 'grey',
          borderBottomWidth: 1,
          width: '85%',
          margin:25
        }}
      />
     <View style={{flexDirection: 'row', justifyContent: 'center', marginVertical:'15%'}}>
         <Text style={styles.foot}>Don't have an account? </Text>
         <TouchableOpacity onPress={()=>navigation.navigate('Register')}><Text style={styles.foot2}>Create new one!</Text></TouchableOpacity>
     </View>

     </> ) }
   </View> 
   
  );
}


const styles = StyleSheet.create({
    text1: {
        padding: 5,
        fontSize: 36,
        marginLeft: '30%',
        marginTop: 70,
        marginBottom: 90,
        fontWeight: 'bold'
        

    },
    field1 : {
        borderWidth:  1,
        borderColor:  'skyblue',
       borderRadius: 50,
       paddingLeft:30,
       paddingBottom:4,
       marginTop:10,
       marginLeft:20,
       marginRight:20,
       marginBottom:10,
       height:45,
       fontSize: 19

       
    },
    field2 : {
        borderWidth:  1,
        borderColor:  'skyblue',
        borderRadius: 50,
        paddingLeft:30,
        paddingBottom:4,
        marginTop:10,
        marginLeft:20,
        marginRight:20,
        marginBottom:10,
        height:45,
        fontSize: 19
        
     },
     rememberme: {
      fontSize:15,
      fontWeight: 'bold',
      paddingLeft:5,
      marginTop: 10,
       marginLeft: -10
     },
     forget: {
        fontSize:13,
        color: 'grey',
        marginLeft: '35%'
     },
    
     foot: {
        padding: 5,
        fontSize: 15,
        marginTop: 100,
       textAlign: 'center',
       color: 'grey'
     },
     foot2: {
      padding: 5,
      fontSize: 15,
      marginTop: 100,
     textAlign: 'center',
     color: 'blue'
   },
  container: {
    flex: 1,
    backgroundColor: 'white',
    // alignItems: 'center',
    // justifyContent: 'center',
  }
}) 