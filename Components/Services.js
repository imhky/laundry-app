import Axios from 'axios';
import React, { useState } from 'react';
import {
  SafeAreaView,
  View,
  StatusBar,
  Text,
  TextInput,
  FlatList,
  Dimensions,
  StyleSheet,
  Image,
  Pressable,
  ScrollView,
  Button , 
  Platform
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ActivityIndicator } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import baseURL from './BaseURL/baseURL';
import ServiceDetails from './ServiceDetails';
const Services = ({route, navigation}) => {
  const [data, setData]  = useState([1, 3, 4, 5, 6, 1, 3, 4, 5, 6])
  const [UserID, setUserID] = useState()
  const { adminID } = route.params;
  const [loading, setLoading] = React.useState(true)
  React.useEffect(() => {
  
    Axios.get(`${baseURL}/serviceManipulate/find/${adminID}`).then(res => {
      // const resp = JSON.stringify(res.data)
      console.log('ALL Details base on iD::::::', res.data)
        setData(res.data[0].Services)
        // setUserID(res.data[0]._id)
        setLoading(false)
    })
    .catch(err => console.log(err))
 
   },[])
  console.log('ADMIN ID ON SERVICE PAGE::::::', adminID)
    return(
    
       <SafeAreaView style=
       {{
         flex:1 , 
         backgroundColor:"white",
         marginTop:'7%'
         
         }}>
       {loading ? (<ActivityIndicator size="large" color="#0f9df7" style={{height: 700}} />) : (<>
           <View  style={{
            
             backgroundColor:"white",
             border: "1px solid #E6DFDF",
             boxSizing: "border-box",
             borderRadius: 5
           }}>
          <View style={{
            flexDirection:"row",

          }}> 
              <TouchableOpacity   onPress={()=>navigation.goBack()}>
               <Icon style={{
               //  height: 50,
                 //width: 50,
                 //backgroundColor:"red",
                 marginTop:13,
                 marginLeft:15,
                 justifyContent: 'center',
                 alignItems: 'center',
               }}
                 name="arrow-back"
                 size={33}
               />
               </TouchableOpacity>
             
         <View style={style.btn}>
           <Text style={{color:"white", fontSize: 18 }}>Choose Services</Text>
         </View>
         </View>
         <ScrollView>
         {
           data.map((data, index) => {
        return(
          <TouchableOpacity activeOpacity={0.8} key={index}
          onPress={()=>navigation.navigate('ServiceDetails', {
            // userID: UserID,
            dhobiID: adminID,
            NormalPrice:data.NormalPrice,
            UrgentPrice:data.UrgentPrice,
            img:data.img,
            title:data.title,
            description:data.description
          })}
          >
          <View  elevation={5}
          style={{
            display: data.Checked === false ? 'none' : 'flex',
            //width: 343,
            width:"95%",
            height: 110,
             left: "2.5%",
             marginTop:"3%",
             marginVertical :"1%",
             // top: 50,
            backgroundColor:"white",
           //  borderWidth:1,
            borderRadius: 5
            
          }}  >
            <View style={{
            flexDirection:"row",
            justifyContent:"space-between",
            marginTop:"-5%",
            marginLeft:'-3%'
          }}>
              <Image key={index + 3}  source={{uri:`${data.img}`}}
              style={style.image} />
            <Text style={{
              position:"absolute",
              width: "auto",
              height: "auto",
              left: "20%",
              top: "110%",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: 17,
              lineHeight: 20,
              color: "#584A4A",
             
            }}>{data.title}</Text>
            </View>
            <Text key={index + 4}  style={{
              position:"absolute",
              width: "80%",
              height: "auto",
              left: "7%",
              top: "55%",
              fontStyle: "normal",
              fontWeight: "normal",
              fontSize: 14,
              lineHeight: 15,
              color: "#777171",
             
              
            }}>{data.description}</Text>
          </View>
          </TouchableOpacity>
        )
             
           })
         }
         </ScrollView>
    
         </View></>)}
        
        
         </SafeAreaView>
       
    );

};
const style = StyleSheet.create({
    
    btn: {
      //position:"absolute",
     // width: 158,
        width:'40%',
      height: 28,
      left:'130%',
      marginBottom: 20,
      top: 15,
      backgroundColor: "rgba(9, 135, 227, 0.75)",
      borderRadius: 20,
      justifyContent: 'center',
      alignItems: 'center',
      marginHorizontal: 20,
        },
    
  image:{
  width: 39,
  height: 39,
  left: 31,
  top: 30,
    },
    
   
  });
  export default Services;