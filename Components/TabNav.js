
import HomeScreen from './HomeScreen';
import ContactUs from './ContactUs';
import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from '@expo/vector-icons/Ionicons';
import UserProfile from "./UserProfile"
import Services from './Services';
import DobiSelection from './DobiSelection'
import ContactUs from './ContactUs';
// (...)
const Tab = createBottomTabNavigator();

export default function TabNavv() {
  return (

      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarHideOnKeyboard: true,
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused
                ? 'ios-home'
                : 'ios-home-outline';
            } else if (route.name === 'My Cart') {
              iconName = focused ? 'ios-cart' : 'ios-cart-outline';
            } else if (route.name === 'Orders') {
              iconName = focused ? 'ios-list' : 'ios-list-outline';
            } else if (route.name === 'Contact Us') {
              iconName = focused ? 'ios-information-circle' : 'ios-information-circle-outline';
            } else if (route.name === 'Profile') {
              iconName = focused ? 'ios-man' : 'ios-man-outline';
            }

            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
          tabBarActiveTintColor: '#0987E3',
          tabBarInactiveTintColor: 'gray',
        })}
      >
        <Tab.Screen name="Home" component={DobiSelection} />
        <Tab.Screen name="My Cart" component={ContactUs} />
        <Tab.Screen name="Orders" component={ContactUs} />
        <Tab.Screen name="Contact Us" component={ContactUs} />
        <Tab.Screen name="Profile" component={UserProfile} />
      </Tab.Navigator>
  );
}
