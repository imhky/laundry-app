import * as React from 'react';
import {View, StyleSheet,
  Image,
  SafeAreaView,
  Text,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Keyboard,
  ToastAndroid

} from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import {Button, TextInput} from 'react-native-paper';
import { Formik } from 'formik'
import * as yup from 'yup'
import { useDispatch, useSelector } from 'react-redux';
import { setLoginDetails, setMessage } from '../Redux/Slice';
import baseURL from './BaseURL/baseURL';

export default function ContactUs({ navigation }) {
  const [name, setName] = React.useState('Haris Khan')
  const [email, setEmail] = React.useState('example@gmail.com')
  const [message, setMessage] = React.useState('Hi, I am developer! Please describe your feedback or suggestion here, Thank you! Regards: Hky')
  const [isButtonDisabled, setIsButtonDisabled] = React.useState(false)
  const [loading, setLoading] = React.useState(true)
  const dispatch = useDispatch();
  const { loginDhobi } = useSelector((state) => state.loginDhobi);


    const feedBackValidationSchema = yup.object().shape({
      name: yup
      .string()
      .matches(/(\w.+\s).+/, 'Enter at least 2 names')
      .required('Full name is required'),
      email: yup
        .string()
        .email("Please enter valid email")
        .required('Email Address is Required'),
          message: yup
          .string()
          .matches(/(\w.+\s).+/, 'Enter at least 2 names')
          .required('Please write some message!'),
    })
  
    return (
      <SafeAreaView>
        <ScrollView>
        <View style={{
          //  backgroundColor: 'rgba(9, 135, 227, 0.74)',
           height: 700
        }}>
        <Image source={require("../Components/images/contact.png")} style={{
          width:"100%",
          height:280
         
        }} />
        </View>
       
          <View elevation={5} style={{
            backgroundColor: 'white',
            position:'absolute',
            marginTop:240,
           
            width:'90%',
            marginLeft: '5%',
            marginRight: '5%',
            // borderWidth:1,
            borderColor: 'grey',
            borderRadius:20

          }}>
    
            <Text style={{
              textAlign: 'center',
              fontWeight: 'bold',
              fontSize: 20,
              margin:20,
              color:'#0987E3'
            }}>GET IN TOUCH!</Text>
                <Formik
   validationSchema={feedBackValidationSchema}
   initialValues={{ name: '', email: '', message: '' }}
   onSubmit={values => {
    setLoading(true)
    console.log('Values::::::', values, loginDhobi._id)
    setIsButtonDisabled(true)
    fetch(`${baseURL}/feedback/post`, {
      method:"POST",
      headers:{
          "Content-Type":"application/json"
      },
      body:JSON.stringify({
              admin_id:loginDhobi._id,
              email:values.email,
              name:values.name,
              message:values.message,
              user_type:'Dhobi'
             
      })
  }).then(res => res.json())
  .then(res => {
    console.log('FEED BACK:::::::', res)
  
      if (res.Result === "Your query placed successfully") {
          console.log(res)
    
               ToastAndroid.showWithGravity(
                "Thank you for Feedback, Will respond soon!",
                ToastAndroid.SHORT,
                ToastAndroid.CENTER
              );
              navigation.navigate('Dashboard')
              setLoading(false)
      }  else if (res === "You are not authenticated!") {
        console.log(res)
             ToastAndroid.showWithGravity(
              "You are not authenticated!",
              ToastAndroid.SHORT,
              ToastAndroid.CENTER
            );
            setLoading(false)
            // navigation.navigate('TabNav')
    }else  {
        ToastAndroid.showWithGravity(
          "Somthing Went Wrong!",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
        setLoading(false)
        // navigation.navigate('TabNav')
      }
   
  }).catch(err => {
      console.log('ERROR :::: ', err)
  })
  setTimeout(() => {
    setIsButtonDisabled(false)
    values.email = '',
    values.name = '',
    values.message = ''
  }, 3000)
   }}
 >
   {({
     handleChange,
     handleBlur,
     handleSubmit,
     values,
     errors,
     isValid,
   }) => (<>
          <TextInput
                     label="Name"
                     name="name"
                     mode="outlined"
                     outlineColor='#0987E3'
                     activeOutlineColor='#0987E3'
                     selectionColor='#0987E3'
                     underlineColor='#0987E3'
                     activeUnderlineColor='#0987E3'
                     style={{marginTop: '1%', marginLeft:'3%', marginRight:'3%'}}
                     onChangeText={handleChange('name')}
                      onBlur={handleBlur('name')}
                      value={values.name}
                      underlineColorAndroid ='transparent'
               />
                 {errors.name &&
         <Text style={{ fontSize: 10, color: 'red', marginLeft:'10%' }}>{errors.name}</Text>
       }
                 <TextInput
                     label="Email"
                     name="email"
                     mode="outlined"
                     outlineColor='#0987E3'
                     activeOutlineColor='#0987E3'
                     selectionColor='#0987E3'
                     underlineColor='#0987E3'
                     activeUnderlineColor='#0987E3'
                     style={{marginTop: '2%', marginLeft:'3%', marginRight:'3%'}}
                     onChangeText={handleChange('email')}
                     onBlur={handleBlur('email')}
                     value={values.email}
                    //  secureTextEntry
                    
                     placeholder='email'
                     underlineColorAndroid ='transparent'
               />
                 {errors.email &&
         <Text style={{ fontSize: 10, color: 'red', marginLeft:'10%' }}>{errors.email}</Text>
       }

                  <TextInput
                     label="Message"
                     name="message"
                     mode="outlined"
                     multiline={true}
                     outlineColor='#0987E3'
                     activeOutlineColor='#0987E3'
                     selectionColor='#0987E3'
                     underlineColor='#0987E3'
                     activeUnderlineColor='#0987E3'
                     style={{marginTop: '2%', marginLeft:'3%', marginRight:'3%', marginBottom:'5%'}}
                     onChangeText={handleChange('message')}
                     onBlur={handleBlur('message')}
                     value={values.message}
                     secureTextEntry
                    
                     placeholder='message'
                     underlineColorAndroid ='transparent'
               />
                {errors.message &&
         <Text style={{ fontSize: 10, color: 'red', marginLeft:'10%' }}>{errors.message}</Text>
       }
               <TouchableOpacity disabled={!isValid  || isButtonDisabled}>
               <Button mode="outlined" color="#0987E3" onPress={() => {
                  handleSubmit()
               }} disabled={!isValid  || isButtonDisabled}
                  style={{
                    // borderColor: '#0987E3',
                    borderColor: (!isValid || isButtonDisabled === true ? '#c4c4c4' : '#0f9df7'),
                    margin:10,
                    marginBottom:'4%'
                }} >
                  SEND
                </Button></TouchableOpacity></>
                 )}
                 </Formik>
          

          </View>
          {/* </TouchableWithoutFeedback>
          </KeyboardAvoidingView> */}

        {/* </View> */}
        </ScrollView>
      </SafeAreaView>
    );
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
