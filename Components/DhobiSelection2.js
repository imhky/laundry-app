import React, { useState } from 'react';
import {
  SafeAreaView,
  View,
  StatusBar,
  Text,
  TextInput,
  FlatList,
  Dimensions,
  StyleSheet,
  Image,
  Pressable,
  ScrollView,
  Button
} from 'react-native';
import Axios from 'axios'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialIcons';
import baseURL from './BaseURL/baseURL';
import { ActivityIndicator, Searchbar } from 'react-native-paper';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Map from './Map'
export default DobiSelection2 = ({navigation}) => {
    const [loading, setLoading] = React.useState(true)
    const [data, setData] = React.useState([
      {
          _id: "61ba2f2144e56cc533e49379",
          username: "ghafar kaka",
          email: "lkml54@gmail.com",
          address: "mardan charsada",
          mobile_no: "+923029463719",
          password: "U2FsdGVkX1+hLdmbWl72btVbmQAbu8P9WOjZBGa95a0=",
          createdAt: "2021-12-15T18:08:33.272Z"
      },
      {
          _id: "61f810a32a22e2f920d1851a",
          username: "sadiq",
          email: "sadiq@gmail.com",
          address: "hangu",
          mobile_no: "03097634512",
          password: "U2FsdGVkX18xAPPiA6WQP2EEsq9f5O7kBKs26Ij8h48=",
          createdAt: "2022-01-31T16:39:00.297Z"
      }
  ])
  
  const [searchQuery, setSearchQuery] = React.useState('');
  const onChangeSearch = query => setSearchQuery(query);
  
    React.useEffect(() => {
    
      Axios.get(`${baseURL}/adminManipulate/getAllAdmines`).then(res => {
        // const resp = JSON.stringify(res.data)
        console.log('ALL DHOBIS::::', res.data)
        setData(res.data)
    
      setLoading(false)
      })
      .catch(err => console.log(err))
   
     },[])
  
     const filteredData = data.filter(item => {
      //  return item.name === 'Zahid Mehmood'
      //  console.log('000000000', item)
      return searchQuery !== "" ? item.username.toLowerCase().includes(searchQuery.toLowerCase()) || item.address.toLowerCase().includes(searchQuery.toLowerCase()) : item 
     
     })
  
      return(
       
          <SafeAreaView style={{
              flex:1 ,
              backgroundColor:"white"
          }}>
            
     {loading ? (<ActivityIndicator size="large" color="#0f9df7" style={{height: 700}} />) : (<>
        <View style=
         {{
          
          backgroundColor:"rgba(9, 135, 227, 0.74)",
          // borderRadius: 10,
          // marginTop:10,
          paddingBottom:8,
          borderBottomEndRadius:30,
          borderBottomStartRadius:30
          
          }}>
          
             <View style={{
                 flexDirection:"row",
                 justifyContent:"space-between",
                 alignItems:"center",
  
             }}>
               <Text style={{
                   width: '90%',
                   height: 25,
                   left: '100%',
                   top: 16,
                   fontStyle: "normal",
                   fontWeight: "bold",
                  fontSize: 24,
                  lineHeight: 26,
                  color: "#FFFFFF"
               }}>Select your Near Dhobi</Text>
             
             </View>
             <View style={{
               top: 14,
               margin:20
             }}>
             <Searchbar
                  placeholder="Search Dhobi Location"
                  onChangeText={onChangeSearch}
                  value={searchQuery}
                />
             </View>
             </View>  
          
             <ScrollView>
            {filteredData.map((el, index) => {
              return (<>
               {/* <TouchableOpacity onPress={()=>navigation.navigate('Services')}></TouchableOpacity> */}
             
             <View elevation={5} key={index + 2} style={{
                 width: '94%',
               
                 left: '3%',
                 top:10,
                 backgroundColor:"white",
                 borderRadius: 10,
                 paddingBottom:5,
                 borderColor:"#584A4A",
                 marginBottom:10
             }}>
                <TouchableOpacity onPress={()=>navigation.navigate('Services', {
              adminID: el._id
            })}>
                 <View  key={index + 3}  style={{
                    //  borderWidth:0.5,
                     borderColor:"#584A4A",
                     marginTop:5,
                     width: '98%',
                    height: 90,
                    borderRadius: 10,  
                      left:'1%',
                 }}>
             <View key={index + 4}  style={{
                 width: 310,
                 height: 140,
             flexDirection:"row",
             justifyContent:"space-between",
           }}>
               <Image source={require("../Components/images/pic5.png")}
               style={{
                  width: 45,
                  height: 45,
                  left: 10,
                  top: 10,
               }} />
             <Text key={index + 5}  style={{
               position:"absolute",
               width: 245,
               height: 36,
               left: 63,
               top: 10,
               fontStyle: "normal",
               fontWeight: "bold",
               fontSize: 16,
               lineHeight: 20,
               color: "#584A4A"
             }}>{el.username}
             </Text>
             <Text key={index + 6}  style={{
               position:"absolute",
               width: 245,
               height: 36,
               left:63,
               top: 25,
               fontStyle: "normal",
               fontWeight: "bold",
               fontSize: 14,
               lineHeight: 20,
               color: "#584A4A"
             }}>{el.email}
             </Text>
             <Text key={index + 7}  style={{
               position:"absolute",
               width: 245,
               height: 36,
               left:61,
               top: 39,
               fontStyle: "normal",
               fontWeight: "bold",
               fontSize: 11,
               lineHeight: 20,
               color: "#584A4A"
             }}> Member since {el.createdAt.substring(0,10)}
             </Text>
          <Text key={index + 8}  style={{
               position:"absolute",
               width: 245,
               height: 36,
               left:15,
               top: 60,
               fontStyle: "normal",
               fontWeight: "bold",
               fontSize: 15,
               lineHeight: 20,
               color: "rgba(1, 122, 209, 0.75)"
             }}>Location: 
             </Text>
             <Text style={{
               position:"absolute",
               width: 245,
               height: 36,
               left:82,
               top: 60,
               fontStyle: "normal",
               fontWeight: "bold",
               fontSize: 14,
               lineHeight: 20,
               color: "black"
             }}>{el.address}
             </Text>
             </View>
           
             </View>
             </TouchableOpacity>
             </View>
             </>)
            })}
                 </ScrollView>   
            
            </>)}
       
          </SafeAreaView>
       
      )
  }
  const style=StyleSheet.create({
      
    });