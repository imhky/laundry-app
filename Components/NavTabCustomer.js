import Services from './Services';
import ContactUs from './ContactUs';
import * as React from 'react';
import { Text, View , SafeAreaView, Platform, BackHandler } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from '@expo/vector-icons/Ionicons';
import NotificationsDhobi from './NotificationsDhobi'
import UserProfile from './UserProfile';
import OrderListDhobi from './OrderListDhobi';
import createService from './createService';
import DobiProfile from './DobiProfile';
import Dashboard from './Dashboard';
import ContactUsDhobi from './ContactUsDhobi'
import Axios from 'axios'

import baseURL from './BaseURL/baseURL';
import { useDispatch, useSelector } from 'react-redux';
import { ActivityIndicator } from 'react-native-paper';

import { setExpoDeviceToekn} from '../Redux/Slice';
// (...)
const Tab = createBottomTabNavigator();

export default function NavTabCustomer() {
  const [loading, setLoading] = React.useState(true)
  const dispatch = useDispatch();
  const { expoDeviceToken } = useSelector((state) => state.expoDeviceToken);
  // let isService = false
  const { loginDhobi } = useSelector((state) => state.loginDhobi);
  const dhobiID = loginDhobi._id
  const Service = loginDhobi.isService
  console.log('LOGIN DHOBI:::::', loginDhobi)
  const onBackButtonPressAndroid = () => {
    return (false);
}
  BackHandler.addEventListener('hardwareBackPress', onBackButtonPressAndroid);

  React.useEffect(() => {
  
      //  send expotoken to backend
        
        Axios.put(`${baseURL}/adminAuth/deviceToken/${loginDhobi._id}`, {
          deviceToken: expoDeviceToken
        })
       .then(res => {
       console.log('Expo not to backend :::::::', res)
    
                 if (res.data.result === "device token added") {
    
                  //  ToastAndroid.showWithGravity(
                  //    `Profile Uploaded successfully!`,
                  //    ToastAndroid.SHORT,
                  //    ToastAndroid.CENTER 
                  //  );
    
                  console.log('DeviceToken sent from dhobbi')
                 } else {
                    // ToastAndroid.showWithGravity(
                    //     `Profile did not Upload, Please try again`,
                    //     ToastAndroid.SHORT,
                    //     ToastAndroid.CENTER
                    //   );
                    console.log('DeviceToken did not send!')
                 }
         
     }).catch(err => {
         console.log('ERROR :::: ', err)
     })
  
    setLoading(false)
 
   },[])
  
 console.log('Service::value:', Service)
  return (
<SafeAreaView style={{
    flex:1 , backgroundColor:"white",
    marginTop:Platform.OS==="android"?25:0
   
}}>
    {loading ? (<ActivityIndicator size="large" color="#0f9df7" style={{top: '50%'}} />) : (
<Tab.Navigator
        screenOptions={({ route }) => ({
          // keyboardHidesTabBar: true,
          tabBarHideOnKeyboard: true,
          tabBarIcon: ({ focused, color, size}) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused
                ? 'ios-home'
                : 'ios-home-outline'; 
            } else if (route.name === 'Dashboard') {
              iconName = focused
                ? 'ios-cellular-sharp'
                : 'ios-cellular-outline'; 
            } else if (route.name === 'Notification') {
              iconName = focused ? 'ios-notifications' : 'ios-notifications-outline';
            } else if (route.name === 'Orders List') {
              iconName = focused ? 'ios-list' : 'ios-list-outline';
            } else if (route.name === 'Contact Us') {
              iconName = focused ? 'ios-chatbubble-ellipses' : 'ios-chatbubble-ellipses-outline';
            } else if (route.name === 'Profile') {
              iconName = focused ? 'ios-person' : 'ios-person-outline';
            }

            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
          tabBarActiveTintColor: '#0987E3',
          tabBarInactiveTintColor: 'gray',
        })}
      >
        <Tab.Screen name={Service === true ? 'Dashboard' : 'Home'} component={Service === true ? Dashboard : createService} />
        <Tab.Screen name="Orders List" component={OrderListDhobi} />
        <Tab.Screen name="Notification" component={NotificationsDhobi} />
  
        <Tab.Screen name="Contact Us" component={ContactUsDhobi} />
        <Tab.Screen name="Profile" component={DobiProfile} />
      </Tab.Navigator> ) }
</SafeAreaView>
      
  );
}
