import Services from './Services';
import ContactUs from './ContactUs';
import * as React from 'react';
import { Text, View , SafeAreaView, Platform, ToastAndroid } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from '@expo/vector-icons/Ionicons';
import Notifications from './Notifications'
import UserProfile from './UserProfile';
import OrderList from './OrderList';

import { useDispatch, useSelector } from 'react-redux';
import { setExpoDeviceToekn} from '../Redux/Slice';
import Axios  from 'axios';
import baseURL from './BaseURL/baseURL';
import TopTab from './TopTab';


// (...)
const Tab = createBottomTabNavigator();

export default function TabNav() {
  const no = 3
  const { expoDeviceToken } = useSelector((state) => state.expoDeviceToken);
  const { login } = useSelector((state) => state.login);
   
  React.useEffect(() => {
  //  send expotoken to backend
    
    Axios.put(`${baseURL}/userAuth/deviceToken/${login._id}`, {
      deviceToken: expoDeviceToken
    })
   .then(res => {
   console.log('Expo not to backend :::::::', res)

             if (res.data.result === "device token added") {

              //  ToastAndroid.showWithGravity(
              //    `Profile Uploaded successfully!`,
              //    ToastAndroid.SHORT,
              //    ToastAndroid.CENTER 
              //  );

              console.log('DeviceToken sent')
             } else {
                // ToastAndroid.showWithGravity(
                //     `Profile did not Upload, Please try again`,
                //     ToastAndroid.SHORT,
                //     ToastAndroid.CENTER
                //   );
                console.log('DeviceToken did not send!')
             }
     
 }).catch(err => {
     console.log('ERROR :::: ', err)
 })
  }, [])
console.log('expo device token::::::', expoDeviceToken)
console.log('User id for expo device token::::::', login._id)
  return (
<SafeAreaView style={{
    flex:1 , backgroundColor:"white",
    marginTop:Platform.OS==="android"?25:0
}}>
<Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarHideOnKeyboard: true,
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused
                ? 'ios-home'
                : 'ios-home-outline';
            } else if (route.name === 'Notifications') {
              iconName = focused ? 'ios-notifications' : 'ios-notifications-outline';
            } else if (route.name === 'Orders List') {
              iconName = focused ? 'ios-list' : 'ios-list-outline';
            } else if (route.name === 'Contact Us') {
              iconName = focused ? 'ios-chatbubble-ellipses' : 'ios-chatbubble-ellipses-outline';
            } else if (route.name === 'Profile') {
              iconName = focused ? 'ios-person' : 'ios-person-outline';
            }
            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
          tabBarActiveTintColor: '#0987E3',
          tabBarInactiveTintColor: 'gray',
        })}
      >
        <Tab.Screen name="Home" component={TopTab} />
        <Tab.Screen name="Orders List" component={OrderList} />
        <Tab.Screen name="Notifications" component={Notifications} options={{ tabBarBadge: `${no}`}} />
     
        <Tab.Screen name="Contact Us" component={ContactUs} />
        <Tab.Screen name="Profile" component={UserProfile} />
      </Tab.Navigator>
</SafeAreaView>
      
  );
}
