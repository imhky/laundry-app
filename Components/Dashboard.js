import React, { useState } from 'react';
import {
  SafeAreaView,
  View,
  StatusBar,
  Text,
  FlatList,
  Dimensions,
  StyleSheet,
  Image,
  Pressable,
  ScrollView,
  Platform,
  ToastAndroid

} from 'react-native';
import { ActivityIndicator } from 'react-native-paper';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ServiceDetails from './ServiceDetails';
import {TextInput, Checkbox, Avatar, Button} from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import Axios  from 'axios';
import baseURL from './BaseURL/baseURL';
const Dashboard = ({navigation}) => {
  const [data, setData] = React.useState([1])
  const { loginDhobi } = useSelector((state) => state.loginDhobi);
  console.log('LOGIN DHOBI::::', loginDhobi)
  const Service = loginDhobi.isService
  const [loading, setLoading] = React.useState(true)
  const [loading2, setLoading2] = React.useState(true)
  const [Inc, setIncr] = React.useState(0)
  const [isButtonDisabled, setIsButtonDisabled] = React.useState(false)
  const [totalOrder, setTotalOrder] = React.useState(0)
  const [processingOrder, setProcessingOrder] = React.useState(0)
  const [cancelledOrder, setCancelledOrder] = React.useState(0)
 const [totalEarning, setTotalEarning] = React.useState(0)
  React.useEffect(() => {
    Axios.get(`${baseURL}/orderManipulate/totalPendingOrders/${loginDhobi._id}`).then(res => {
      // const resp = JSON.stringify(res.data)
      console.log('ALL Orders of Dhobi::::::', res.data.orderRelate)
      if (res.data.length === 0 || res.data === undefined || res.data === []) {
        console.log('No Order')
        ToastAndroid.showWithGravity(
          "You don't have any Pending Order!",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
        setData([])
        setLoading(false)
        setLoading2(false)
      } else {
        console.log('Orders ::D:::', res.data)
        setData(res.data)
        setLoading(false)
        // setLoading2(false)
      }
 
    })
    .catch(err => console.log(err))
 
    
    Axios.get(`${baseURL}/orderManipulate/noOfOrder/${loginDhobi._id}`).then(res => {
      console.log('Orders::::::', res.data)
      setTotalOrder(res.data)
    }).catch(err => console.log(err))

    Axios.get(`${baseURL}/orderManipulate/processingOrder/${loginDhobi._id}`).then(res => {
        console.log('P Orders::::::', res.data)
        setProcessingOrder(res.data)
      }).catch(err => console.log(err))

      Axios.get(`${baseURL}/orderManipulate/cancelledOrder/${loginDhobi._id}`).then(res => {
        console.log('C Orders::::::', res.data)
        setCancelledOrder(res.data)
      }).catch(err => console.log(err))

      Axios.get(`${baseURL}/orderManipulate/totalIncome/${loginDhobi._id}`).then(res => {
        console.log('C Orders::::::', res.data)
        setTotalEarning(res.data)
      }).catch(err => console.log(err))
   },[Inc])

   const tConv24 = (time24) => {
    var ts = time24;
    var H = +ts.substr(0, 2);
    var h = (H % 12) || 12;
    h = (h < 10)?("0"+h):h;  // leading 0 at the left for 1 digit hours
    var ampm = H < 12 ? " AM" : " PM";
    ts = h + ts.substr(2, 3) + ampm;
    return ts;
  }
  const changeStatus = (id) => {
    setIsButtonDisabled(true)
    Axios.put(`${baseURL}/orderManipulate/update/${id}`, {
      order_status: 'PROCESSING'
    })
    .then(res => {
    console.log('Status Chnage Res :::::::', res.data)

              if (res.data.order_status === "PROCESSING") {

                ToastAndroid.showWithGravity(
                  `Order Status Changed Successfully`,
                  ToastAndroid.SHORT,
                  ToastAndroid.CENTER
                );
                setIncr(Inc + 1)
              } else {

                ToastAndroid.showWithGravity(
                  `Something Went Wrong`,
                  ToastAndroid.SHORT,
                  ToastAndroid.CENTER
                );
              }
      
  }).catch(err => {
      console.log('ERROR :::: ', err)
  })
  setTimeout(() => {
    setIsButtonDisabled(false)
  }, 3000)
  }

  const changeStatusCancel = (id) => {
    setIsButtonDisabled(true)
    Axios.put(`${baseURL}/orderManipulate/update/${id}`, {
      order_status: 'CANCELED'
    })
    .then(res => {
    console.log('Status Chnage Res :::::::', res.data)

              if (res.data.order_status === "CANCELED") {

                ToastAndroid.showWithGravity(
                  `Order Canceled Successfully`,
                  ToastAndroid.SHORT,
                  ToastAndroid.CENTER
                );
              } else {

                ToastAndroid.showWithGravity(
                  `Something Went Wrong`,
                  ToastAndroid.SHORT,
                  ToastAndroid.CENTER
                );
              }
      
  }).catch(err => {
      console.log('ERROR :::: ', err)
  })
  setTimeout(() => {
    setIsButtonDisabled(false)
  }, 3000)
  }

    return(
        <SafeAreaView>
            <View style={{
              top:0,
                width:'100%',
                backgroundColor: '#0987E3',
                height: Service === true ? 140 : 195,
                borderRadius:15
            }}>
                <View style={{
                    flexDirection: 'row',
                    // justifyContent: 'space-evenly'
                    display: Service === true ? 'none' : 'flex'
                }}>
            
              <Icon style={{
                 top:36,
                 left: '17%'
               }}
               color="white"
                 name={Service === true ? '' : "arrow-back"}
                 size={33}
                 onPress={()=>navigation.goBack()}
               />
                <Text style={{
                    top:26,
                    color: 'white',
                    textAlign: 'center',
                    fontSize: 25,
                    margin:10,
                    width:'60%',
                    marginLeft:'10%',
                    marginRight:'10%',
                    display: Service === true ? 'none' : 'flex'
                }}>Dhobi Dashboard</Text>
                </View>
              
                <View style={{
                    flexDirection: 'row', justifyContent: 'space-around', top:15
                }}>

                    <Text style={{
                    top:5,
                    color: 'white',
                    textAlign: 'center',
                    fontSize: 20,
                    margin:10
                }}>Order Frequency : <Text style={{fontWeight: 'bold'}}> {loginDhobi.frequency_order} / Day</Text></Text>
                {/* <Button mode="outlined" color="white" onPress={()=>navigation.navigate('createService')}
                
                style={{
                    borderColor: 'white',
                    margin:10
                }} >
                 Edit
                </Button> */}
                </View>
                <Button mode="outlined" color="white" onPress={()=>navigation.navigate('updateService')}
                
                style={{
                    borderColor: 'white',
                    margin:10,
                    width:'88%',
                    marginLeft:'6%',
                    marginRight:'6%',
                    top:15
                }} >
                 Edit your Services
                </Button>
            </View>
          
            <View style={{
                width:'100%',
                backgroundColor: '#f2f2f2',
                height: Service === true ? '81%' : '73%'
            }}>
  <ScrollView>
                {/* First Row */}
              <View style={{flexDirection: 'row', justifyContent:'space-evenly'}}>
               <View elevation={3} style={{
                   
                   width:'45%',
                   borderColor: 'grey',
                   borderRadius:20,
                   backgroundColor: 'white',
                   top: 10
               }}>
                    <Icon style={{
                 top:15,
                 left:16
               }}
               color="green"
                 name="trending-up"
                 size={33}
               />
                    <Text style={{
                        color: '#0987E3',
                        fontSize:30,
                        margin:10,
                        left:10,
                        top:17,
                        fontWeight: 'bold'
                    }}>{totalOrder}</Text>
                <Text  style={{
                    color: '#a69e88',
                        fontSize:20,
                        top:0,
                        left:5,
                        
                        margin:10
                    }}>Total Order</Text>
                
               </View>
               <View elevation={3} style={{
                   
                   width:'45%',
                   borderColor: 'grey',
                   borderRadius:20,
                   backgroundColor: 'white',
                   top: 10
               }}>
                    <Icon style={{
                 top:15,
                 left:16
               }}
               color="green"
                 name="vertical-align-center"
                 size={33}
               />
                    <Text style={{
                        color: '#0987E3',
                        fontSize:30,
                        margin:10,
                        left:10,
                        top:17,
                        fontWeight: 'bold'
                    }}>{processingOrder}</Text>
                <Text  style={{
                    color: '#a69e88',
                        fontSize:20,
                        top:0,
                        left:0,
                        
                        margin:10,
                        textAlign: 'center'
                    }}>InProgress Order</Text>
                
               </View>
              </View>

              {/* 2nd row */}

              <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
               <View elevation={3} style={{
                   
                   width:'45%',
                   borderColor: 'grey',
                   borderRadius:20,
                   backgroundColor: 'white',
                   top: 22
               }}>
                    <Icon style={{
                 top:15,
                 left:16
               }}
               color="green"
                 name="view-compact"
                 size={33}
               />
                    <Text style={{
                        color: '#0987E3',
                        fontSize:30,
                        margin:10,
                        left:10,
                        top:17,
                        fontWeight: 'bold'
                    }}>{cancelledOrder}</Text>
                <Text  style={{
                    color: '#a69e88',
                        fontSize:20,
                        top:0,
                        left:5,
                        
                        margin:10,
                    }}>Cancelled Order</Text>
                
               </View>
               <View elevation={3} style={{
                   
                   width:'45%',
                   borderColor: 'grey',
                   borderRadius:20,
                   backgroundColor: 'white',
                   top: 22
               }}>
                    <Icon style={{
                 top:15,
                 left:16
               }}
               color="green"
                 name="money"
                 size={33}
               />
                    <Text style={{
                        color: '#0987E3',
                        fontSize:30,
                        margin:10,
                        left:10,
                        top:17,
                        fontWeight: 'bold'
                    }}>555</Text>
                <Text  style={{
                    color: '#a69e88',
                        fontSize:20,
                        top:0,
                        left:0,
                        
                        margin:10,
                        textAlign: 'center'
                    }}>Completed Order</Text>
               </View>
              </View>

              
              {/* 3rd row */}

              <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
               {/* <View elevation={3} style={{
                   
                   width:'45%',
                   borderColor: 'grey',
                   borderRadius:20,
                   backgroundColor: 'white',
                   top: 35
               }}>
                    <Icon style={{
                 top:15,
                 left:16
               }}
               color="green"
                 name="view-compact"
                 size={33}
               />
                    <Text style={{
                        color: '#0987E3',
                        fontSize:30,
                        margin:10,
                        left:10,
                        top:17,
                        fontWeight: 'bold'
                    }}>009</Text>
                <Text  style={{
                    color: '#a69e88',
                        fontSize:20,
                        top:0,
                        left:5,
                        
                        margin:10,
                    }}>Completed Order</Text>
                
               </View> */}
               <View elevation={3} style={{
                   
                   width:'45%',
                   borderColor: 'grey',
                   borderRadius:20,
                   backgroundColor: 'white',
                   top: 35
               }}>
                    <Icon style={{
                 top:15,
                 left:16
               }}
               color="green"
                 name="money"
                 size={33}
               />
                    <Text style={{
                        color: '#0987E3',
                        fontSize:30,
                        margin:10,
                        left:10,
                        top:17,
                        fontWeight: 'bold'
                    }}>Rs: {totalEarning}</Text>
                <Text  style={{
                    color: '#a69e88',
                        fontSize:20,
                        top:0,
                        left:0,
                        
                        margin:10,
                        textAlign: 'center'
                    }}>Total Earning</Text>
               </View>
              </View>
   
              {/* PENDINGORDERS */}
              {/* {loading2 ? (<ActivityIndicator size="large" color="#0f9df7" style={{height: 700}} />) : 
              (<View><Text style={{
         display: loading ? 'flex' : 'none',
         backgroundColor: '#0f9df7',
         textAlign:'center',
         color: 'white',
         fontWeight:'bold',
         fontSize: 20,
         padding:10,
         marginLeft:'5%',
         marginRight:'5%',
         marginTop:30,
         marginBottom:'1%',
         borderRadius: 20
       }}>No Order here, Please go back!</Text></View>)} */}
       {loading ? (<ActivityIndicator size="large" color="#0f9df7" style={{height: 700}} />) : (<View>
              <View style={{
            width: '94%',
            height: 36,
            left: '3%',
            right: '3%',
            top: 50,
            backgroundColor: "grey",
            justifyContent:"center",
            alignItems:"center",
        }}>
          <Text style={{
            textAlign:"center",
            width: 173,
            height: 36,
            marginTop:10,
            fontStyle: "normal",
            fontWeight: "bold",
            fontSize: 17,
            lineHeight: 23,
            color: "#FFFFFF"
          }}>Pending Orders</Text>
          </View> 
          {
            //3rd parent Portion ends
        }
          {
              //4rth Parent portion starts
          }
         {
            data.map((data, index) => {
              console.log('DATA::::::::', data)
              return(
               <View style={{
                 borderWidth:0.5,
                 borderColor:"#584A4A",
                 marginTop:10,
                 top:50,
                 width: '94%',
                height: 130,
                borderRadius: 10,  
                  left:'3%',
                  right: '3%',
                  marginBottom: 1,
                  // (data.status === 'PENDING' ? 'flex' : 'none')
                  display: loading2 === false ? 'none' : 'flex'
             }}>
                 {
            // 1st Inner view starts
          }
          <View style={{
             width: '90%',
             height: 140,
          flexDirection:"row",
          justifyContent:"space-between",
          }}>
           <Image source={require("../Components/images/pic5.png")}
           style={{
              width: 32,
              height: 32,
              left: 10,
              top: 15,
           }} />
          <Text style={{
           position:"absolute",
           width: 245,
           height: 36,
           left: 50,
           top: 10,
           fontStyle: "normal",
           fontWeight: "bold",
           fontSize: 12,
           lineHeight: 20,
           color: "#584A4A"
          }}>Order No.  {data === 1  ? '' : data._id.substring(0,5)}
          </Text>
          <Text style={{
           position:"absolute",
           width: 245,
           height: 36,
           left:50,
           top: 25,
           fontStyle: "normal",
           fontWeight: "bold",
           fontSize: 12,
           lineHeight: 20,
           color: "#584A4A"
          }}>{data === 1 ? '' : data.orderRelate[0].username}
          </Text>
          <Text style={{
           position:"absolute",
           width: 245,
           height: 36,
           left:50,
           top: 39,
           fontStyle: "normal",
           fontWeight: "bold",
           fontSize: 12,
           lineHeight: 20,
           color: "#584A4A"
          }}> {data === 1 ? '' : data.orderRelate[0].email}
          </Text>
          {/* <TouchableOpacity disabled={isButtonDisabled} onPress={() => {
            changeStatus(data._id)}}> */}
          <View style={{
          width: 88,
          height: 28,
          left:'130%',
          top: 10,
          backgroundColor:"rgba(1, 122, 209, 0.75)",
          marginHorizontal: 20,
          borderWidth: 0.5,
          borderColor:"rgba(1, 122, 209, 0.75)",
          borderRadius:50,
          justifyContent: 'center',
          alignItems: 'center'
          }}>
            <TouchableOpacity disabled={isButtonDisabled} onPress={() => {
            changeStatus(data._id)}}>
             <Text style={{color: 'white'}}>Accept</Text>
             </TouchableOpacity>
         
          </View>
          {/* </TouchableOpacity> */}
          <Pressable>
          <View style={{
          width: 76,
          height: 28,
          left:40,
          top: 55,
          backgroundColor:"rgba(254, 8, 8, 0.75)",
          marginHorizontal: 20,
          borderWidth: 0.5,
          borderRadius:50,
          borderColor:"#E31A1A",
          justifyContent: 'center',
          alignItems: 'center',
          }}>
              <TouchableOpacity disabled={isButtonDisabled} onPress={() => {
            changeStatusCancel(data._id)}}>
          <Text style={{color: 'white'}}>Cancel</Text>
          </TouchableOpacity>
          </View>
          </Pressable>
          <Text style={{
           position:"absolute",
           width: 245,
           height: 36,
           left:15,
           top: 60,
           fontStyle: "normal",
           fontWeight: "bold",
           fontSize: 14,
           lineHeight: 20,
           color: "rgba(1, 122, 209, 0.75)"
          }}>Total : Rs. {data === 1 ? '' : data.order_price}   
          </Text>
          <Text style={{
           position:"absolute",
           width: 245,
           height: 36,
           left:15,
           top: 75,
           fontStyle: "normal",
           fontWeight: "bold",
           fontSize: 14,
           lineHeight: 20,
           color: "black"
          }}>Date: {data === 1 ? '' : data.order_pickDate.substring(0,10)} & Time: {data === 1 ? '' : tConv24(data.order_pickTime.substring(11,16))} 
          </Text>
          </View>
          {
            // 1st Inner view ends
          }
          {
            // 2nd Inner view starts
          }
          <View style={{
             flexDirection:"row",
             // justifyContent:"space-between",
             alignItems:"center"
          }}>
             <Image source={require("../Components/images/pic7.png")}
           style={{
              width: 20,
              height: 25,
              left: 10,
              top:-50,
           }} />
           <Text style={{
               width: 245,
               height: 43, 
               top:-35,
               left:20,                
               fontStyle: "normal",
               fontWeight: "normal",
               fontSize: 11,
               lineHeight: 13,
               color: "#846262"
           }}>{data.order_address}</Text>
          </View>
          {
            // 2nd Inner view ends
          }
          </View> )})
         }
         
          </View>)} 
           </ScrollView>
          
              {/*  */} 
            </View>
          
           
        </SafeAreaView>
    )
}

export default Dashboard;