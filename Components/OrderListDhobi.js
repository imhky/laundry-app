import React from 'react';
import {
  SafeAreaView,
  View,
  StatusBar,
  Text,
  TextInput,
  FlatList,
  Dimensions,
  StyleSheet,
  Image,
  Pressable,
  ScrollView,
  Button,
  ToastAndroid,
  RefreshControl
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import baseURL from './BaseURL/baseURL';
import Icon from 'react-native-vector-icons/MaterialIcons';
import  Axios  from 'axios';
import { ActivityIndicator } from 'react-native-paper';
const OrderList = ({navigation}) => {
  const [data, setData] = React.useState([1])
  const [loading, setLoading] = React.useState(true)
  const [loading2, setLoading2] = React.useState(true)
  const [Inc, setIncr] = React.useState(0)
  const { loginDhobi } = useSelector((state) => state.loginDhobi);
  const dhobiID = loginDhobi._id
//  const dhobiID = '61ba2f2144e56cc533e49379'
const loadUserData = () => {
  setLoading(true)
  setIncr(Inc + 1)
}

  React.useEffect(() => {
  Axios.get(`${baseURL}/orderManipulate/totalOrderesList/${dhobiID}`).then(res => {
    // const resp = JSON.stringify(res.data)
    console.log('ALL Orders of Dhobi::::::', res.data.orderRelate)
    if (res.data.length === 0 || res.data === undefined || res.data === []) {
      console.log('No Order')
      ToastAndroid.showWithGravity(
        "You don't have any Order!",
        ToastAndroid.SHORT,
        ToastAndroid.CENTER
      );
      
      setLoading2(false)
      setLoading(false)
    } else {
      console.log('Orders ::D:::', res.data)
      setData(res.data)
      setLoading(false)
      // setLoading2(false)
    }
      // setData(res.data)
      // setLoading(false)
  })
  .catch(err => console.log(err))
 
 

 },[Inc])
 const tConv24 = (time24) => {
  var ts = time24;
  var H = +ts.substr(0, 2);
  var h = (H % 12) || 12;
  h = (h < 10)?("0"+h):h;  // leading 0 at the left for 1 digit hours
  var ampm = H < 12 ? " AM" : " PM";
  ts = h + ts.substr(2, 3) + ampm;
  return ts;
}
    return(
        <SafeAreaView style={{
            flex:1 ,
            backgroundColor:"white"
          
        }}>
              {/* {loading2 ? (<ActivityIndicator size="large" color="#0f9df7" style={{height: 700}} />) : (<Text style={{
         display: loading ? 'flex' : 'none',
         backgroundColor: '#0f9df7',
         textAlign:'center',
         color: 'white',
         fontWeight:'bold',
         fontSize: 20,
         padding:10,
         marginLeft:'5%',
         marginRight:'5%',
         marginTop:30,
         marginBottom:'1%',
         borderRadius: 20
       }}>No Order here, Please go back!</Text>)} */}
       {loading ? (<ActivityIndicator size="large" color="#0f9df7" style={{height: 700}} />) : ( <View><View style=
       {{
        
        backgroundColor:"rgba(9, 135, 227, 0.74)",
        borderBottomEndRadius:50,
        borderBottomStartRadius:50,
        marginTop:0,
        paddingBottom:10,
        
        }}>
       
           </View>  
           <ScrollView refreshControl={
        <RefreshControl refreshing={loading} onRefresh={loadUserData} />
      }>
           <View elevation={5} style={{
               width: "95%",
               left: 10,
               //top:-75,
               top: 5,
               backgroundColor:"white",
               borderRadius: 10,
               paddingBottom:30,
               display: loading2 === false ? 'none' : 'flex'

               
           }}>
     
             {data.map((data, index) => {
               return(
               
               <View  style={{
                  borderWidth:0.5,
                   marginTop:5,
                   width: "97.3%",
                  height: "22%",
                  borderRadius: 10,
                  borderColor:'grey',  
                    left:5,  
                    height:110
               }}>
          
           <View  style={{
               width: 310,
               height: 140,
           flexDirection:"row",
           justifyContent:"space-between",
         }}>
             <Image key={index + 4} source={require("../Components/images/pic5.png")}
             style={{
                width: 42,
                height: 42,
                left: 10,
                top: 10,
             }} />
           <Text key={index + 5} style={{
             position:"absolute",
             width: 245,
             height: 36,
             left: 60,
             top: 10,
             fontStyle: "normal",
             fontWeight: "bold",
             fontSize: 15,
             lineHeight: 20,
             color: "#584A4A"
           }}>Order No. {data === 1 ? '' : data._id.substring(0,5)}
           </Text>
           <Text key={index + 6} style={{
             position:"absolute",
             width: 245,
             height: 36,
             left:60,
             top: 25,
             fontStyle: "normal",
             fontWeight: "bold",
             fontSize: 12,
             lineHeight: 20,
             color: "#584A4A"
           }}>{data === 1 ? '' : data.orderRelate[0].username}
           </Text>
           <Text key={index + 7} style={{
             position:"absolute",
             width: 245,
             height: 36,
             left:60,
             top: 39,
             fontStyle: "normal",
             fontWeight: "bold",
             fontSize: 12,
             lineHeight: 20,
             color: "#584A4A"
           }}>Order Type: {data.order_type}
           </Text>
           <Pressable>
          <View style={{
           width: "90%",
           height: 28,
           right: data.order_status === 'PROCESSING' ? '-80%' : '-83%',
            top: "9%",
            backgroundColor:"rgba(1, 122, 209, 0.75)",
            marginHorizontal: 20,
            borderWidth: 0.5,
            borderColor:"rgba(1, 122, 209, 0.75)",
        //borderColor: "rgba(9, 135, 227, 0.74)",
        borderRadius:50,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            <Text key={index + 8} style={{color: 'white'}}>{data.order_status === 'delivered' ? 'DELIVERED' : (data.order_status === 'PROCESSING' ? 'PENDING' : (data.order_status === 'onHold' ? 'On Hold' : (data.order_status === 'cancelled' ? 'CANCELLED' : data.order_status)))}</Text>
          </View>
        </Pressable>
        <Pressable>
          <View disabled={data.order_status === 'onHold' ? true : false} style={{
            
            // display: data.order_status === 'delivered' || data.order_status === 'cancelled' ? 'none' : 'flex',
            display: 'none',
           width: "86%",
           height: "20%",
           left:"7%",
            top: "35%",
            backgroundColor:"rgba(254, 8, 8, 0.75)",
            marginHorizontal: "9%",
            borderWidth: 0.5,
        //borderColor: "rgba(9, 135, 227, 0.74)",
        borderRadius:50,
        borderColor:"#E31A1A",
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            <Text  key={index + 9} style={{color: 'white'}}>Cancel</Text>
          </View>
        </Pressable>
        <Text key={index + 10} style={{
             position:"absolute",
             width: 245,
             height: 36,
             left:15,
             top: 60,
             fontStyle: "normal",
             fontWeight: "bold",
             fontSize: 14,
             lineHeight: 20,
             color: "rgba(1, 122, 209, 0.75)"
           }}>Total : Rs. {data.order_price}   
           </Text>
           <Text key={index + 12} style={{
             position:"absolute",
             width: 245,
             height: 36,
             left:15,
             top: 75,
             fontStyle: "normal",
             fontWeight: "bold",
             fontSize: 14,
             lineHeight: 20,
             color: "black"
           }}>Date: {data === 1 ? '' : data.order_pickDate.substring(0,10)} & Time: {data === 1 ? '' : tConv24(data.order_pickTime.substring(11,16))}
           </Text>
           </View>
           </View>
          )
                  })}
                 
          
           </View>
           </ScrollView> 
           </View>)}
           
        </SafeAreaView>
        
    )
}
const style=StyleSheet.create({
    
  });
export default OrderList;