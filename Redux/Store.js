import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { AsyncStorage } from 'react-native';
import persistReducer from 'redux-persist/es/persistReducer';
import persistStore from 'redux-persist/es/persistStore';
import messageReducer from './Slice';
import LoginReducer from './Slice'
import LoginDhobiReducer from './Slice'
import logger from 'redux-logger'
import ExpoDeviceTokenReducer  from './Slice';

const persistConfig = {
	key: "root",
	version: 1,
	storage: AsyncStorage,
}

const rootReducer = combineReducers({
  message: messageReducer,
  login: LoginReducer,
  loginDhobi: LoginDhobiReducer,
  expoDeviceToken: ExpoDeviceTokenReducer
})

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
  reducer: persistedReducer,
  // middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger), // non serilization ... error/warning
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      thunk: {
        extraArgument: '',
      },
      serializableCheck: false,
    }),
});

export const persistor = persistStore(store);


// export const store = configureStore({
//   reducer: {
//     message: messageReducer,
//     login: LoginReducer,
//     loginDhobi: LoginDhobiReducer
//   }
// });