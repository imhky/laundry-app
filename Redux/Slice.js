import { createSlice, PayloadAction } from "@reduxjs/toolkit"

const messageSlice = createSlice({
  name: "message",
  initialState: {
    message: "Initial message is that ",
    login: {},
    loginDhobi: {},
    expoDeviceToken: ""
  },
  reducers: {
    setMessage(state, action) {
      state.message = action.payload
    },
    setLoginDetails(state, action) {
      state.login = action.payload
    },
    setDhobiLoginDetails(state, action) {
      state.loginDhobi = action.payload
    },
    setExpoDeviceToken(state, action) {
      state.expoDeviceToken = action.payload
    },

  }
})

export const { setMessage, setLoginDetails, setDhobiLoginDetails, setExpoDeviceToken} = messageSlice.actions
export default messageSlice.reducer
